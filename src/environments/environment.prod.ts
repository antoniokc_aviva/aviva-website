export const environment = {
  production: true,
  url: 'https://api.aviva.pe/middleware2/',
  urlBaseAlter: 'https://api.aviva.pe/middleware2/',
  urlImagenes: 'https://api.aviva.pe/middleware2/img/professionals/',
  urlApiNodos: 'https://apiws.aviva.pe/',
  apiKeyCulqi: 'pk_live_CyArY9ygzb0d7oZb',
  agora: {
    appId: 'b75a830a11a84a0bbb812fc81aa60860'
  },
 firebaseConfig: {
    apiKey: "AIzaSyDzPQPjYiipYkBF3z4J7iJlmhgpAgAMSu8",
    authDomain: "doctorestele.firebaseapp.com",
    databaseURL: "https://doctorestele.firebaseio.com",
    projectId: "doctorestele",
    storageBucket: "doctorestele.appspot.com",
    messagingSenderId: "172467271749",
    appId: "1:172467271749:web:3b7e5deb0109aba21acb2c",
    measurementId: "G-WJXQ00YZR8"
  }
 /*  production: true,
  url: 'https://dappapache02.eastus.cloudapp.azure.com/middleware2/',
  urlBaseAlter: 'https://dappapache02.eastus.cloudapp.azure.com/middleware2/',
  urlImagenes: 'https://dappapache02.eastus.cloudapp.azure.com/middleware2/img/professionals/',
  urlApiNodos: 'https://apiwsdev.aviva.pe/',
  apiKeyCulqi: 'pk_test_e85SD7RVrWlW0u7z',
  agora: {
    appId: 'b75a830a11a84a0bbb812fc81aa60860'
  },
  firebaseConfig: {
    apiKey: "AIzaSyDzPQPjYiipYkBF3z4J7iJlmhgpAgAMSu8",
    authDomain: "doctorestele.firebaseapp.com",
    databaseURL: "https://doctorestele.firebaseio.com",
    projectId: "doctorestele",
    storageBucket: "doctorestele.appspot.com",
    messagingSenderId: "172467271749",
    appId: "1:172467271749:web:3b7e5deb0109aba21acb2c",
    measurementId: "G-WJXQ00YZR8"
  } */
};