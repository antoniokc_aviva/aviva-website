import { Component, OnInit } from '@angular/core';
import { ReservasService } from '../reservas.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { fadeIn } from '../../shared/animations/animation';
import { GoogleTagManagerService } from 'angular-google-tag-manager';


@Component({
  selector: 'app-pago',
  templateUrl: './pago.component.html',
  animations: [fadeIn]
})
export class PagoComponent implements OnInit {

  preloader: boolean = false;
  public inLocalPay: boolean = false;
  public color: any = 'warn';
  public mode: any = 'indeterminate';
  public loader: boolean = false;
  public tokenCulqi;

  public price;

  public appoiemendIdd;
  public financiador;

  progressPage = {
    page: 'avivaCuida',
    state: 'pago',
    pageDoctor: true,
    pageRegistro: true,
    pageSeguro: true
  }

  public messageAlert;

  constructor(public dialog: MatDialog, private reservasService: ReservasService, private router: Router, private gtmService: GoogleTagManagerService) { }

  ngOnInit() {

    this.financiador = this.reservasService.financiador;
    if (this.reservasService.priceReser != '') {

      this.price = this.reservasService.priceReser;

      this.reservasService._progressPage.next(this.progressPage);

    } else {
      this.router.navigate(['/'])
    }

  }
  
  tag() {
    const gtmTag = {
      event: 'page',
      pageName: 's'
    };
    this.gtmService.pushTag(gtmTag);
    
  }

  backLink() {
    window.history.back();
  }

  showEvent(even) {
    this.loader = even;

  }

}
