import { Injectable } from '@angular/core';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { AuthService } from '../shared/auth/auth.service';
import { HttpClient, HttpResponse, HttpRequest, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})

export class ReservasService {



  _progressObs: Subject<any> = new Subject();
  _progressPage: Subject<any> = new Subject();

  public dataPlansCliente;
  public dataPlansClienteId;
  public prestacion;
  public dateCita;
  public parent;
  public parentId;
  public infoCita;
  public idParent;
  public dependens;
  public financiador;

  public urlPdfWhatssap: string = '';
  public dataJson: any = [];
  public id = 1;
  public provisionsId;
  public dataCreateParent;

  public priceReser: any = '';
  public provisions: any;
  private url = environment.urlBaseAlter;

  constructor(private Authservice: AuthService, private http: HttpClient) { }

  urlBaseNodos = this.Authservice.getUrlBaseNodos();
  urlBase = this.Authservice.getUrlBase();
  get progressObs() {
    return this._progressObs;
  }
  get progressPage() {
    return this._progressPage;
  }

  getSpecialty() {
    console.log('data')
    return this.http
      .get(this.urlBase + 'api/v2/ebooking/fmt-centers/all/services');
  }

  getSpecialtyTele() {
    console.log('data')
    const tele = 845337
    return this.http
      .get(this.urlBase + `api/v2/ebooking/fmt-centers/${tele}/services`);
  }

  getAllDoctors(){
    return this.http.get(this.urlBase + `/api/v2/ebooking/info-profesionales`).pipe(
      map( data => {
        return data
      }, err => {
        return err
      })
    )
  }


  getDoctorsSpecialty(params: any, date1: any, date2: any) {
    return this.http
      .get(this.urlBase + 'api/v2/ebooking/fmt-centers/1/services/' + params + '/professionals/all/availables?from_date=' + date1 + '&to_date=' + date2);
  }

  getDoctorsSpecialtyTele(params: any, date1: any, date2: any) {
    const session = JSON.parse(localStorage.getItem('session'));
    let headers = new HttpHeaders({ "Authorization": session.authorization });

    return this.http
      .get(this.url + `api/v2/ebooking/fmt-centers/1/basicservices/${params}/professionals/all/provision/845337/availables?from_date=${date1}&to_date=${date2}`, { headers });
  }

  createAppoitment() {
    console.log(this.provisionsId)
    console.log(this.dataJson)
    this.dataJson.provisions = [{ "default": false, "id": this.provisionsId }];
    return this.http
      .post(this.urlBase + 'api/v2/ebooking/appointments/create', this.dataJson);
  }

  createParentDate() {
    console.log('dataJson en createParent', this.dataJson);
    const session = JSON.parse(localStorage.getItem('session'));
    let headers = new HttpHeaders({ "Authorization": session.authorization });
    let params = this.dataJson;
    this.dataJson.provisions = [{ "default": false, "id": this.provisionsId }];

    return this.http.post(this.urlBase + `api/v2/ebooking/appointments/createForUser/${this.parentId}`, params, { headers }).pipe(
      map((resp: any) => {
        return resp;
      })
    )
  }

  startPayCulqi(data: any) {
    return this.http
      .post(this.urlBase + 'api/v2/ebooking/culqi-charges', data);
  }

  getPlansFinanciador(service_id, doctorId, fecha) {
    return this.http
      .get(this.urlBase + 'api/v2/ebooking/planes-paciente-precio-prestacion?center_id=1&servicio_id=' + service_id + '&prestacion_id=' + this.provisionsId + '&medico_id=' + doctorId + '&fecha=' + fecha + ' ')
  }

  getplanesContacto(paciente_id, servicio_id, prestacion_id, medico_id, proposed_date) {
    const session = JSON.parse(localStorage.getItem('session'));
    let headers = new HttpHeaders({ "Authorization": session.authorization });

    return this.http.get(this.urlBase + `api/v2/ebooking/planes-paciente-contacto-precio-prestacion?paciente_id=${paciente_id}&servicio_id=${servicio_id}&prestacion_id=${this.provisionsId}&medico_id=${medico_id}&fecha=${proposed_date}`, { headers }).pipe(
      map(data => {
        return data
      })
    )
  }

  delteCita(cita) {
    console.log('delete cita principal', cita)
    const appointmentId = cita.appointmentId;
    return this.http
      .delete(this.urlBase + 'api/v2/ebooking/appointments/' + appointmentId + ' ')
  }

  destroyAppointmentContact(appointment) {
    console.log('delete cita parent:', appointment);
    const patientId = appointment.patientId;
    const appointmentId = appointment.appointmentId;
    console.log('lo que se va a eliminar cita fmiliar', patientId, appointmentId);
    const session = JSON.parse(localStorage.getItem('session'));
    let headers = new HttpHeaders({ "Authorization": session.authorization });

    return this.http.delete(this.urlBase + `api/v2/ebooking/appointments/patient-contacts/${patientId}/${appointmentId}/`, { headers }).pipe(
      map((resp: any) => {
        return resp;
      })
    )
  }

  createParent(data) {
    const session = JSON.parse(localStorage.getItem('session'));
    let headers = new HttpHeaders({ "Authorization": session.authorization });
    let params = data;

    return this.http.post(this.urlBase + `api/v2/users/register-dependent/`, params, { headers }).pipe(
      map((resp: any) => {
        return resp;
      })
    )
  }

  validateCita() {
    this.dataJson.provisions = [{ "default": false, "id": `${this.provisionsId}` }];
    return this.http
      .post(this.urlBase + 'api/v2/ebooking/appointments/validate', this.dataJson)
  }

  saveCitaNod(data) {
    return this.http
      .post(this.urlBaseNodos + 'appointments/reserve', data)
  }

  getPdf() {
    return this.http
      .post(this.urlPdfWhatssap, this.id)
  }

  getDoctorsPerId(id) {
    const authorization = localStorage.getItem('authorization');
    let headers = new HttpHeaders({ "Authorization": authorization });
    const center_id = 1;

    return this.http.get(this.urlBase + `ebooking/fmt-centers/${center_id}/services/${id}/professionals`, { headers }).pipe(
      map((resp: any) => {
        return resp
      })
    )
  }

  chekstatusAppointment(appointmentId) {
    const authorization = localStorage.getItem('authorization');
    let headers = new HttpHeaders({ "Authorization": authorization });

    return this.http.get(this.urlBase + `api/v2/ebooking/appointments/${appointmentId}/status`, { headers }).pipe(
      map(resp => {
        return resp
      })
    )
  }


  chekstatusAppointmentParent(patientId, appointmentId) {
    const authorization = localStorage.getItem('authorization');
    let headers = new HttpHeaders({ "Authorization": authorization });

    return this.http.get(this.urlBase + `api/v2/ebooking/appointments-contact/${patientId}/${appointmentId}/status`, { headers }).pipe(
      map(resp => {
        return resp
      })
    )
  }

  confirmDate(appointmentId) {
    console.log('appointment en servicio confirm',appointmentId);
    const authorization = localStorage.getItem('authorization');
    let headers = new HttpHeaders({ "Authorization": authorization });
    /* let params = [{data:'data'}]; */

    return this.http.post(this.urlBase + `api/v2/ebooking/appointments/${appointmentId}/confirm`, { headers }).pipe(
      map(resp => {
        return resp
      })
    )
  }

  confirmDateParent(patientId, appointmentId) {
    const authorization = localStorage.getItem('authorization');
    let headers = new HttpHeaders({ "Authorization": authorization });
    let params = "";

    return this.http.post(this.urlBase + `api/v2/ebooking/appointments-contact/${patientId}/${appointmentId}/confirm`, params, { headers }).pipe(
      map(resp => {
        return resp
      })
    )
  }


}