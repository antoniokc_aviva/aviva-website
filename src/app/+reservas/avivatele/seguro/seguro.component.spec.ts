import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeguroTeleComponent } from './seguro.component';

describe('SeguroTeleComponent', () => {
  let component: SeguroTeleComponent;
  let fixture: ComponentFixture<SeguroTeleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SeguroTeleComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeguroTeleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
