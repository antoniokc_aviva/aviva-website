import { Component, OnInit } from '@angular/core';
import { ReservasService } from '../../reservas.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { fadeIn } from 'src/app/shared/animations/animation';


@Component({
  selector: 'app-pago',
  templateUrl: './pago.component.html',
  styleUrls: ['./pago.component.scss'],
  animations: [fadeIn]
})
export class PagoTeleComponent implements OnInit {

  preloader: boolean = false;
  public inLocalPay: boolean = false;
  public color: any = 'warn';
  public mode: any = 'indeterminate';
  public loader: boolean = false;
  public tokenCulqi;

  public price;

  public appoiemendIdd;
  public reservaData;
  public prestacion;
  public parent;

  progressPage = {
    page: 'avivaTele',
    state: 'pago',
    pageDoctor: true,
    pageRegistro: true,
    pageSeguro: true
  }

  constructor(public dialog: MatDialog,
    private reservasService: ReservasService,
    private router: Router,
    private routes: ActivatedRoute) { }

  ngOnInit() {
    console.log('datos de reserva:', this.reservaData);
    if (this.reservasService.priceReser != '') {
      this.price = this.reservasService.priceReser;
      this.prestacion = this.reservasService.prestacion;
      this.reservasService._progressPage.next(this.progressPage);
      this.parent = this.reservasService.parent;
      console.log('this.prestacion:', this.prestacion, this.parent);
    } else {
      this.router.navigate(['/'])
    }
  }

  backLink() {
    window.history.back();
  }

  showEvent(even) {
    this.loader = even;

  }

}
