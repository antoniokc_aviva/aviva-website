import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagoTeleComponent } from './pago.component';

describe('PagoTeleComponent', () => {
  let component: PagoTeleComponent;
  let fixture: ComponentFixture<PagoTeleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PagoTeleComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagoTeleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
