import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroTeleComponent } from './registro.component';

describe('RegistroTeleComponent', () => {
  let component: RegistroTeleComponent;
  let fixture: ComponentFixture<RegistroTeleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RegistroTeleComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroTeleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
