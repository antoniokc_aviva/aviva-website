import { Component, OnInit } from '@angular/core';
import { fadeIn } from '../../shared/animations/animation'
import { ReservasService } from '../reservas.service';
import { AuthService } from 'src/app/shared/auth/auth.service';

@Component({
  selector: 'app-avivatele',
  templateUrl: './avivatele.component.html',
  styleUrls: ['./avivatele.component.scss'],
  animations: [fadeIn]
})
export class AvivateleComponent implements OnInit {
  public dataSpecialty: any;
  public preloader: boolean;
  public color: any = 'warn';
  public mode: any = 'indeterminate';
  urlBaseAlter;

  progressPage = {
    page: 'avivaTele',
    state: 'especialidad',
    pageDoctor: false
  }

  constructor(private auth: AuthService, private reservasService: ReservasService) { }

  ngOnInit() {
    this.urlBaseAlter = this.auth.urlBaseAlter;
    this.preloader = true;
    //- STATE BAR PROGRESS
    this.reservasService.progressPage.next(this.progressPage);

    //-DATA SERVICE SPECIALTY
    this.reservasService.getSpecialty()
      .subscribe((data: any) => {
        if (data) {
          this.preloader = false
        }
        console.log(data);
        this.dataSpecialty = data.centers[0].services;
        console.log('dataSpecialty:', this.dataSpecialty);
        this.dataSpecialty.forEach(element => {
          element.trackingId = this.eliminarDiacriticos(element.description);
          element.trackingId = element.trackingId.split(" ").join("-");
          element.trackingId = element.trackingId.toLowerCase();
        });
        const filter = this.dataSpecialty.filter(x => x.tele === 'yes');
        this.dataSpecialty = filter;
      }, (erro: any) => {

      })

  }

  getSpecialtyTele() {
    this.reservasService.getSpecialtyTele().subscribe((data: any) => {
      console.log('data de servuicio tele:', data);
    })
  }

  eliminarDiacriticos(texto) {
    return texto.normalize('NFD').replace(/[\u0300-\u036f]/g, "");
  }

}
