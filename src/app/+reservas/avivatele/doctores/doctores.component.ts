import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import 'moment/locale/es'
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ReservasService } from '../../reservas.service';
import { fadeIn } from 'src/app/shared/animations/animation';
import { ModalDetailsDoctorsComponent } from 'src/app/shared/modal/modal-details-doctors/modal-details-doctors.component';
import { AuthService } from 'src/app/shared/auth/auth.service';





@Component({
  selector: 'app-doctores',
  templateUrl: './doctores.component.html',
  styleUrls: ['./doctores.component.scss'],
  animations: [fadeIn]
})
export class DoctoresTeleComponent implements OnInit {
  // progressPage = 'doctores';
  progressPage = {
    page: 'avivaTele',
    state: 'doctores',
    pageRedy: true
  }
  public color: any = 'warn';
  public mode: any = 'indeterminate';

  pageReady = true;
  stateShoww: boolean;
  boxID: any = null;
  boxCaID: any = null;

  public progress = 75;
  public id: any;
  public dataDoctors: any;
  public preloader: boolean;
  public manyBoxes: any;

  public speciallty: string;

  public urlBase;

  datesCalendar: any;

  provisionsData = '845337';

  // INFORMATION TO SERVICE

  public dateFirst = moment().format('YYYY-MM-DD');
  public dateSecond = moment().add(6, 'days').format('YYYY-MM-DD');

  constructor(private dialog: MatDialog,
    private reservasService: ReservasService,
    private activeRoute: ActivatedRoute,
    private auth: AuthService,
    private routes: Router) { }

  ngOnInit() {
    console.log('doctores tele');
    this.urlBase = this.auth.urlBaseAlter;
    this.reservasService._progressPage.next(this.progressPage);
    this.preloader = true;
    this.activeRoute.params.subscribe(routeParams => {
      this.id = routeParams.id;
      this.speciallty = routeParams.description.split("-").join(" ");
    });

    this.reservasService.getDoctorsSpecialtyTele(this.id, this.dateFirst, this.dateSecond)
      .subscribe((data: any) => {

        this.provisionsData = data.centers[0].services[0].provision;

        if (data) { this.preloader = false }

        var start = Date.now();

        const docts = data.centers[0].services[0].professionals.filter((element) => {
          return element.availables.length > 0;
        })

        this.manyBoxes = docts.length;

        docts.forEach(element => {
          const fech = element.availables;
          this.datesCalendar = fech;

          fech.forEach(dat => {
            dat.hours.hour = dat.hours.map((element: any) => {
              return element.hour.slice(0, 5);
            });
            dat.newFormatDay = moment(dat.date).locale('es').format('DD');
            dat.date = moment(dat.date).locale('es').format('dddd').slice(0, 3);
          });

        });
        this.dataDoctors = docts;
        console.log('dataDoctors:', this.dataDoctors);

        var end = Date.now();

      }, (error: any) => {
      })

  }

  stateShow(item: any, index) {
    this.boxID = item;
    this.boxCaID = index;
  }

  redirectTo(info, index, doctor, provisionsID) {
    console.log('ab', info, index, doctor, provisionsID);

    this.reservasService.provisionsId = provisionsID[0];
    console.log('provision in doctor:', this.reservasService.provisionsId);
    const listjson = this.dataDoctors[doctor].availables[index].hours[info].listjson;
    console.log('lo que tiene e listJson:', listjson);
    const newJson = JSON.parse(listjson);

    this.reservasService.dataJson = newJson;
    this.reservasService.dateCita = moment(newJson.appointmentDateTime).locale('es').format('LLLL');
    /* newJson.provisions = [this.provisionsData]; */




    const session = JSON.parse(localStorage.getItem('session'));

    if (session.role === 'user') {
      this.routes.navigate(['/reservas/avivatele/seguro']);
    } else {
      this.routes.navigate(['/reservas/avivatele/registro']);
    }
  }


  // OPEN MODAL LOGIN
  openLogin(data): void {
    const diallogRef = this.dialog.open(ModalDetailsDoctorsComponent, {
      data: {

        page: 'aviva-tele',
        infoDetails: data

      },
      width: 'auto'
    });
    diallogRef.afterClosed().subscribe(res => {

    })
  }

  backLink() {
    window.history.back();
  }

}
