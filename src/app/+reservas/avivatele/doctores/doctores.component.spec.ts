import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctoresTeleComponent } from './doctores.component';

describe('DoctoresComponent', () => {
  let component: DoctoresTeleComponent;
  let fixture: ComponentFixture<DoctoresTeleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DoctoresTeleComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctoresTeleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
