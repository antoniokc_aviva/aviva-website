import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReservasRoutingModule } from './reservas-routing.module';
import { ReservasComponent } from './reservas.component';
import { AvivacuidaComponent } from './avivacuida/avivacuida.component';
import { ReservasComponentContent } from './reservas-contet/reservas-content.component';
import { DoctoresComponent } from './doctores/doctores.component';
import { HttpClientModule } from '@angular/common/http';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { RegistroCitaComponent } from './registro/registro-cita.component';
import { SeguroComponent } from './seguro/seguro.component';
import { PagoComponent } from './pago/pago.component';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import { AvivateleComponent } from './avivatele/avivatele.component';

import { ButtonPayComponent } from '../shared/utils/button-pay/button-pay.component';
import { MatRadioModule, } from '@angular/material';
import { ValidateCodeComponent } from './validate-code/validate-code.component';
import { DoctoresTeleComponent } from './avivatele/doctores/doctores.component';
import { SeguroTeleComponent } from './avivatele/seguro/seguro.component';
import { RegistroTeleComponent } from './avivatele/registro/registro.component';
import { PagoTeleComponent } from './avivatele/pago/pago.component';



@NgModule({
  declarations: [
    ReservasComponent,
    AvivacuidaComponent,
    ReservasComponentContent,
    DoctoresComponent,
    RegistroCitaComponent,
    SeguroComponent,
    PagoComponent,
    ValidateCodeComponent,
    ButtonPayComponent,
    AvivateleComponent,
    DoctoresTeleComponent,
    SeguroTeleComponent,
    RegistroTeleComponent,
    PagoTeleComponent
  ],
  imports: [
    CommonModule,
    ReservasRoutingModule,
    HttpClientModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatButtonModule,
    FormsModule
  ],
  exports: [
    ReservasComponent,
    AvivacuidaComponent,
    ReservasComponentContent,
    DoctoresComponent,
    RegistroCitaComponent,
    SeguroComponent,
    PagoComponent,
    ValidateCodeComponent,
    ButtonPayComponent,
    AvivateleComponent,
    DoctoresTeleComponent,
    SeguroTeleComponent,
    RegistroTeleComponent,
    PagoTeleComponent

  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ReservaModule { }
