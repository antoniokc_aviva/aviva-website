import { Component, OnInit } from '@angular/core';
import { ReservasService } from '../../reservas.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { MatDialog } from '@angular/material';
import { alertComponent } from '../../../shared/modal/alert/alert.component';
/* import { containsElement } from '@angular/animations/browser/src/render/shared'; */
import { fadeIn } from '../../../shared/animations/animation';
import { GoogleTagManagerService } from 'angular-google-tag-manager';

declare const window: any;

@Component({
  selector: 'app-pago',
  templateUrl: './pago.component.html',
  animations: [fadeIn]
})
export class PagoComponent implements OnInit {

  public color: any = 'warn';
  public mode: any = 'indeterminate';
  public loader: boolean = false;
  public price;
  public financiador;



  progressPage = {
    page: 'avivaCura',
    state: 'pago',
    pageDoctor: true,
    pageRegistro: true,
    pageSeguro: true
  }

  public messageAlert;

  constructor(public dialog: MatDialog, private reservasService: ReservasService, private router: Router, private gtmService: GoogleTagManagerService) { }

  ngOnInit() {

    this.financiador = this.reservasService.financiador;
    console.log(this.financiador);

    if (this.reservasService.priceReser != '') {
      this.price = this.reservasService.priceReser;
      this.reservasService._progressPage.next(this.progressPage);
    } else {
      this.router.navigate(['/'])
    }
  }

  tag() {
    const gtmTag = {
      event: 'page',
      pageName: 's'
    };
    this.gtmService.pushTag(gtmTag);
    
  }

  backLink() {
    window.history.back();
  }

  showEvent(even) {
    this.loader = even;

  }

}
