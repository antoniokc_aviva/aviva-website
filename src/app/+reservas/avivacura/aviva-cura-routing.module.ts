import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AvivacuraComponent } from './avivacura.component'

//COMPONENT FLUJO AVIVA CURA
import { DoctoresComponent } from './doctores/doctores.component';
import { RegistroCitaComponent } from './registro/registro-cita.component';
import { SeguroComponent } from './seguro/seguro.component';
import { PagoComponent } from './pago/pago.component';
import { ValidateCodeComponent } from './validate-code/validate-code.component';

const routes: Routes = [
  {
    path: '',
    component: AvivacuraComponent
  },
  {
    path: 'selecciona-doctor/:id/:description',
    component: DoctoresComponent
  },
  {
    path: 'valida-codigo',
    component: ValidateCodeComponent
  },
  {
    path: 'seguro',
    component: SeguroComponent
  },
  {
    path: 'registro',
    component: RegistroCitaComponent
  },
  {
    path: 'pago',
    component: PagoComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AvivaCuraRoutingModule { }
