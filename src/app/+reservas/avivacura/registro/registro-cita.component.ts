import { Component, OnInit } from '@angular/core';
import { ReservasService } from '../../reservas.service';
import { MatDialog } from '@angular/material';
import { ModalComponent  } from '../../../shared/modal/modal/modal.component';
import { RegisterModalComponent  } from '../../../shared/modal/register-modal/register-modal.component';
import { fadeIn } from '../../../shared/animations/animation'

@Component({
  selector: 'app-registro-cita',
  templateUrl: './registro-cita.component.html',
  animations: [fadeIn]
})
export class RegistroCitaComponent implements OnInit {
  
  progressPage = {
    page : 'avivaCura',
    state :  'registro',
    pageDoctor : true
  }

  dateCita;

  constructor(private reservasService : ReservasService, public dialog: MatDialog) { }

  ngOnInit() {
    this.dateCita = this.reservasService.dateCita;
    this.reservasService._progressPage.next(this.progressPage);

  }

  // OPEN MODAL LOGIN
  openLogin(): void{
    const diallogRef = this.dialog.open(ModalComponent, {
      data: 'aviva-cura'
    });
    diallogRef.afterClosed().subscribe(res => {
   
    })
  }

  // OPEN MODAL REGISTER

  openRegister(): void{
    const diallogRef = this.dialog.open(RegisterModalComponent, {
      data: 'aviva-cura',
      panelClass: ['aviva-cura-fondo'] 
    });
    diallogRef.afterClosed().subscribe(res => {
     
    })
  }
  backLink(){
    window.history.back();
  }
}

