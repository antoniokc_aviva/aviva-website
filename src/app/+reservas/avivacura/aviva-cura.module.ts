import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AvivacuraComponent } from './avivacura.component';
import { AvivaCuraRoutingModule } from './aviva-cura-routing.module';

//COMPONENT FLUJO AVIVA CURA
import { DoctoresComponent } from './doctores/doctores.component';
import { RegistroCitaComponent } from './registro/registro-cita.component';
import { SeguroComponent } from './seguro/seguro.component';
import { PagoComponent } from './pago/pago.component';
import { ValidateCodeComponent } from './validate-code/validate-code.component';

import {MatButtonModule} from '@angular/material/button';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

import { ReservaModule } from '../reserva.module';

import {
  MatCheckboxModule,
  MatRadioModule,
  } from '@angular/material';

@NgModule({
  declarations: [
    AvivacuraComponent,
    DoctoresComponent,
    RegistroCitaComponent,
    SeguroComponent,
    PagoComponent,
    ValidateCodeComponent
  ],
  imports: [
    CommonModule,
    AvivaCuraRoutingModule,
    MatCheckboxModule,
    MatRadioModule,
    MatButtonModule,
    FormsModule,
    HttpClientModule,
    MatProgressSpinnerModule,
    ReservaModule
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
})
export class AvivaCuraModule { }
