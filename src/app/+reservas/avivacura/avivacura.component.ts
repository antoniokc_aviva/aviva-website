import { Component, OnInit } from '@angular/core';
import { ReservasService } from '../reservas.service';
import { fadeIn } from '../../shared/animations/animation'
import {  AuthService } from '../../shared/auth/auth.service'

@Component({
  selector: 'app-avivacura',
  templateUrl: './avivacura.component.html',
  styleUrls: ['./avivacura.component.sass'],
  animations: [fadeIn]
})
export class AvivacuraComponent implements OnInit {
  page = 'avivaCura';
  public dataSpecialty: any;
  public preloader: boolean;
  public color: any = 'warn';
  public mode: any = 'indeterminate';
  public urlBaseAlter;


  progressPage = {
    page : 'avivaCura',
    state :  'especialidad',
    pageDoctor : false
  }

  public space = '-';
  public patron = " ";

  constructor(private auth: AuthService,private reservasService: ReservasService) { }

  ngOnInit() {

    this.urlBaseAlter = this.auth.urlBaseAlter;
    this.preloader = true;
    //- STATE BAR PROGRESS
    this.reservasService.progressPage.next(this.progressPage);

    //- DATA SERVICE SPECIALTY
    
    this.reservasService.getSpecialty()
    .subscribe((data: any) => {

 
      if(data){ this.preloader = false}
 
      this.dataSpecialty = data.centers[0].services.filter((element) =>{
        return element.block === 'cura'                
      })

      this.dataSpecialty.forEach(element => {

       element.trackingId = this.eliminarDiacriticos(element.description);
       element.trackingId = element.trackingId.split(" ").join("-");
       element.trackingId = element.trackingId.toLowerCase()
        
      });

      // this.dataSpecialty = data.centers[0].services.filter((element) =>{
      //   return element.newData = this.eliminarDiacriticos(element.description)
                
      // })

      // this.dataSpecialty = data.centers[0].services.filter((element) =>{
      //   return element.newData = element.description.split(" ").join("-");
      // })
    }, (error: any) => {
 
    })
  }

  eliminarDiacriticos(texto){
    return texto.normalize('NFD').replace(/[\u0300-\u036f]/g,"");
}

backLink(){
  window.history.back();
}

}
