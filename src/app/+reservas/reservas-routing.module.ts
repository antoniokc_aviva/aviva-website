import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReservasComponent } from './reservas.component';
import { AvivacuidaComponent } from './avivacuida/avivacuida.component';
import { ReservasComponentContent } from './reservas-contet/reservas-content.component'
import { DoctoresComponent } from './doctores/doctores.component';
import { RegistroCitaComponent } from './registro/registro-cita.component';
import { SeguroComponent } from './seguro/seguro.component';
import { PagoComponent } from './pago/pago.component';
import { UserGuard } from '../shared/auth/guard/user.guard';
import { desactivateUser } from '../shared/auth/guard/desactiva.guard';
import { ValidateCodeComponent } from './validate-code/validate-code.component';
import { AvivateleComponent } from './avivatele/avivatele.component';
import { DoctoresTeleComponent } from './avivatele/doctores/doctores.component';
import { SeguroTeleComponent } from './avivatele/seguro/seguro.component';
import { PagoTeleComponent } from './avivatele/pago/pago.component';
import { RegistroTeleComponent } from './avivatele/registro/registro.component';



const routes: Routes = [
  {
    path: '',
    component: ReservasComponent,
    children: [{
      path: '', component: ReservasComponentContent
    }]
  },
  {
    path: 'avivacuida',
    component: ReservasComponent,
    children: [{
      path: '', component: AvivacuidaComponent
    }]
  },
  {
    path: 'avivacura',
    component: ReservasComponent,
    loadChildren: () => import('src/app/+reservas/avivacura/aviva-cura.module').then(m => m.AvivaCuraModule)

  },
/*   {
    path: 'avivatele',
    component: ReservasComponent,
    children: [{
      path: '', component: AvivateleComponent
    }]
  }, */
  {
    path: 'avivacuida/selecciona-doctor/:id/:description',
    component: ReservasComponent,
    children: [{
      path: '', component: DoctoresComponent
    }
    ]
  },
  {
    path: 'avivatele/selecciona-doctor/:id/:description',
    component: ReservasComponent,
    children: [{
      path: '', component: DoctoresTeleComponent
    }
    ]
  },
  {
    path: 'avivatele/registro',
    canActivate: [desactivateUser],
    component: ReservasComponent,
    children: [{
      path: '', component: RegistroTeleComponent
    }
    ]
  },
  {
    path: 'avivacuida/registro',
    canActivate: [desactivateUser],
    component: ReservasComponent,
    children: [{
      path: '', component: RegistroCitaComponent
    }
    ]
  },
  {
    path: 'avivacuida/valida-codigo',
    canActivate: [desactivateUser],
    component: ReservasComponent,
    children: [{
      path: '', component: ValidateCodeComponent
    }
    ]
  },
  {
    path: 'avivacuida/seguro',
    canActivate: [UserGuard],
    component: ReservasComponent,
    children: [{
      path: '', component: SeguroComponent
    }
    ]
  },
  {
    path: 'avivatele/seguro',
    canActivate: [UserGuard],
    component: ReservasComponent,
    children: [{
      path: '', component: SeguroTeleComponent
    }
    ]
  },
  {
    path: 'avivacuida/pago',
    canActivate: [UserGuard],
    component: ReservasComponent,
    children: [{
      path: '', component: PagoComponent
    }
    ]
  },
  {
    path: 'avivatele/pago',
    canActivate: [UserGuard],
    component: ReservasComponent,
    children: [{
      path: '', component: PagoTeleComponent
    }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReservasRoutingModule { }
