import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MisCitasDetailsComponent } from './mis-citas-details.component';

describe('MisCitasDetailsComponent', () => {
  let component: MisCitasDetailsComponent;
  let fixture: ComponentFixture<MisCitasDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MisCitasDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MisCitasDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
