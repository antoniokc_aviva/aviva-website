import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidateCodePasswordComponent } from './validate-code-password.component';

describe('ValidateCodePasswordComponent', () => {
  let component: ValidateCodePasswordComponent;
  let fixture: ComponentFixture<ValidateCodePasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidateCodePasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidateCodePasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
