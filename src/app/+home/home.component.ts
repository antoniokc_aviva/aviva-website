import { Component, OnInit } from '@angular/core';
import { fadeIn, SlideAnimation } from '../shared/animations/animation'
import { AuthService } from '../shared/auth/auth.service';
import { MatDialog } from '@angular/material';
import { ModalComponent } from '../shared/modal/modal/modal.component';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { ReservasService } from '../+reservas/reservas.service';
import { style } from '@angular/animations';
import { Router } from '@angular/router';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';

export interface Doctor {
  fullname: string;
  id: string;
  idSpecialty: string;
  services: any;
  imageUrl: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations:[SlideAnimation]
})
export class HomeComponent implements OnInit {
  defaultImage = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/AABEIAAcACgMBEQACEQEDEQH/xAGiAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgsQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+gEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoLEQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/APqaP4a/twfCLwn4jn/ag8P/AA78YWum61H42tNE8N3vwn0DXtX8Y26+FpZNR1FND+FF/wCHrLVtP0/wo3hu28RaR4j03Vb7RtVWK8utmmaell8zKcKEVD3rxXJyJuMab+GM4cui5Xy2Vn8K1VrHtSnXxlWricT/ALRia9edeviKjVStiKtSTnUnXqVFz1Ks23z1JTc5OTcpSvdfE9t+0R8RJra3mh/Zy8E6TFLBFJFpVz470ie50yN41ZNPuJ7Dwh9gmnslItpZbL/RJJI2e2/clK8aecYKE5wlia/NCUoy9yb96LaevJrqtz9ZwXg3x3j8HhMdhcmy6eGxuFoYvDzlmeCpylQxNKFalKUHO8JSpzi3B6xbs9Uf/9k=';
  public user: string;
  public show: boolean;
  state: boolean = false;
  public page: any = 'home';



  public loaderSession: boolean = false;
  public buscador;
  serviceError;
  public doctors;
  public selectDoctor;
  public myControl = new FormControl();
  name: string;
  public urlBase;
  options: Doctor[] = [];
  filteredOptions: Observable<Doctor[]>;
  constructor(private AuthService : AuthService, 
              public dialog: MatDialog, 
              public reservaSrv: ReservasService, 
              public router: Router,
              private gtmService: GoogleTagManagerService){ 
                
              }

  ngOnInit() {
    this.urlBase = this.AuthService.urlBaseAlter;
    this.getAllDoctors();
    this.AuthService._positionPage.next(this.page);
    
    this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value.fullname),
        map(fullname => fullname ? this._filter(fullname) : this.options.slice())
        );
      console.log('my control:',this.myControl.valueChanges);
  }
  displayFn(doctor: Doctor): string {
    return doctor && doctor.fullname ? doctor.fullname : '';
  }

  private _filter(fullname: String): Doctor[] {
    const filterValue = fullname.toLowerCase();
    return this.options.filter(option => option.fullname.toLowerCase().includes(filterValue));
  }
  

  tag() {
    const gtmTag = {
      event: 'page',
      pageName: 's'
    };
    this.gtmService.pushTag(gtmTag);
    
  }

  getAllDoctors(){
    this.reservaSrv.getAllDoctors().subscribe((data:any) => {
      this.options = data; 
      console.log(this.doctors);
    }, err => {
      console.log(err)
    })
  }
  
  changeState(){
    this.state = true;
  }

  isUser(){
    if(this.AuthService.isUser() === 'user'){
      this.user = this.AuthService.User();
      this.show = true;
      return true
    }else{
      this.user = '';
      this.show = false;
      return false
    }
  }

  logout(){
    if(this.AuthService.logout()){
      this.AuthService.getSesionPublic();
    
    }else{
    }
  }

    // OPEN MODAL LOGIN
    openLogin(): void{
    const diallogRef = this.dialog.open(ModalComponent, {
      data: 'home'
    });
    diallogRef.afterClosed().subscribe(res => {

    })
  }

  openModalManteince(){
  }

  onkeyValidateString(){

  }

  goToDoctor(item){
    console.log('ir a doctor:', item);
    const id =item.id;
    const especialtyId = item.services[0].id;
    console.log(id, especialtyId);
    this.router.navigateByUrl(`/especialidades-doctores/details-doctors/${especialtyId}/${id}`)
  }
  
  

}
