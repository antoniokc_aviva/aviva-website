import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProgramasRoutingModule } from './planes-programas-routing.module';
import { PlanesProgramasComponent } from './planes-programas.component';
import { PlanesProgramasDetailsComponent } from './planes-programas-details/planes-programas-details.component';
import {HttpClientModule} from '@angular/common/http';
import { BoxTableComponent } from './planes-programas-details/box-table/box-table.component';
import { BoxTabComponent } from './planes-programas-details/box-tab/box-tab.component';
import { BoxTabTabComponent } from './planes-programas-details/box-tab-tab/box-tab-tab.component';
import { BoxCarComponent } from './planes-programas-details/box-car/box-car.component';
import { PlanVivaComponent } from './plan-viva/plan-viva.component';

import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
  
@NgModule({
  declarations: [
    PlanesProgramasComponent,
    PlanesProgramasDetailsComponent,
    BoxTableComponent,
    BoxTabComponent,
    BoxTabTabComponent,
    BoxCarComponent,
    PlanVivaComponent
  ],
  imports: [
    CommonModule,
    ProgramasRoutingModule,
    HttpClientModule,
    MatProgressSpinnerModule
  ],
  exports: [
    BoxTableComponent,
    BoxTabComponent,
    BoxTabTabComponent,
    BoxCarComponent,
  ]
})
export class PlanesProgramasModule { }
