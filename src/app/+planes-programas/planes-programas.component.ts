import { Component, OnInit } from '@angular/core';
import { PlanesProgramasService } from './planes-programas.service';
import { AuthService } from '../shared/auth/auth.service';
import { fadeIn } from '../shared/animations/animation';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-planes-programas',
  templateUrl: './planes-programas.component.html',
  animations:[fadeIn]
})
export class PlanesProgramasComponent implements OnInit {

  public dataPlans: any;
  public stateNav = false;
  public preloader = true;

  public color: any = 'accent';
  public mode: any = 'indeterminate';

  constructor(private planesServices: PlanesProgramasService, public auth: AuthService) { }

  ngOnInit() {
    
    this.preloader = true;
    this.auth.stateNav.next(this.stateNav)
    this.planesServices.getPlans().subscribe((data: any) => {

    this.preloader = false;
    const planesFiltrados = data.data.filter(x => x.id === '1' || x.id === '2');
    //estamos limitando por front la salida de los planes niño sano y chequeo preventivo
    /* this.dataPlans = data.data; */
    this.dataPlans = planesFiltrados;

      console.log(this.dataPlans, planesFiltrados);
    }, error => {

    })
    
  }

}
