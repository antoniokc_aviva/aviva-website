import { Component, OnInit, OnDestroy } from '@angular/core';
import { PlanesProgramasService } from '../planes-programas.service'
import { AuthService } from '../../shared/auth/auth.service';
import { ModalReservaPlanesComponent } from '../../shared/modal/modal-reserva-planes/modal-reserva-planes.component';
import { MatDialog } from '@angular/material';
import { fadeIn } from '../../shared/animations/animation';

import { Router, ActivatedRoute } from '@angular/router';
import { BreakpointObserver } from '@angular/cdk/layout';

@Component({
  selector: 'app-plan-viva',
  templateUrl: './plan-viva.component.html',
  styleUrls: ['./plan-viva.component.sass'],
  animations: [fadeIn]
})
export class PlanVivaComponent implements OnInit, OnDestroy {

  public dataPlans: any;
  public preloader = false;

  public color: any = 'accent';
  public mode: any = 'indeterminate';

  public plansId;

  public stateNav = true;
  public newData = {

    name: '',
    interiorDescription: '',
    leftContent: '',
    images: {
      main: ''
    },
    planServices: [{
      planPrograms: [
        {
          html: ''
        }
      ]
    }],
    planDocuments: [
      {
        filename: '',
        name: ''
      }
    ]


  };

  public main = '';

  isDesktop: boolean;
  
  IMAGES = {
    ONE: {
      width: '375',
      height: '391'
    }
  }

  constructor(private breakpointObserver: BreakpointObserver, public activate : ActivatedRoute ,public dialog: MatDialog, public planesService: PlanesProgramasService, private auth: AuthService) { }

  ngOnInit() {
    
    this.auth.stateNav.next(this.stateNav)

    this.activate.params.subscribe((data: any) => {
      console.log(data)
      this.preloader = true;

      this.planesService.getplanVivaMas(data.slug).subscribe((data: any) => {
        this.plansId = data.data.id
        this.preloader = false;
  
        this.newData = data.data;
     

      
      })
    } )

    this.breakpointObserver
    .observe(['(min-width: 1200px)'])
    .subscribe((result) => this.responsive(result.matches));

 
  }

  responsive(matches: boolean) {

    this.IMAGES.ONE = {
      width: matches ? '1920' : '375',
      height: matches ? '675' : '391',
    }

    

  }

  ngOnDestroy(){
    this.stateNav = false;
    this.auth.stateNav.next(this.stateNav )
  }
     // OPEN MODAL LOGIN
     openLogin(data, extra): void{

     const diallogRef = this.dialog.open(ModalReservaPlanesComponent, {
       data : {
         extra: extra ? extra : '',
         page: 'home',
         idPlans: data
       }
     });
     diallogRef.afterClosed().subscribe(res => {
       
     })
   }

}
