import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanVivaComponent } from './plan-viva.component';

describe('PlanVivaComponent', () => {
  let component: PlanVivaComponent;
  let fixture: ComponentFixture<PlanVivaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanVivaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanVivaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
