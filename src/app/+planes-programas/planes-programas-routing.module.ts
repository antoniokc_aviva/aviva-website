import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlanesProgramasComponent } from './planes-programas.component';
import { PlanesProgramasDetailsComponent } from './planes-programas-details/planes-programas-details.component';

// COMPONENT
import { PlanVivaComponent } from './plan-viva/plan-viva.component';

const routes: Routes = [
  {
    path:'',
    component: PlanesProgramasComponent,
  },
  {
    path:':slug',
    component: PlanVivaComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgramasRoutingModule { }
