import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxCarComponent } from './box-car.component';

describe('BoxCarComponent', () => {
  let component: BoxCarComponent;
  let fixture: ComponentFixture<BoxCarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoxCarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxCarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
