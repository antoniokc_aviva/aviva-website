import { Component, OnInit, Input } from '@angular/core';
import { ModalReservaPlanesComponent } from '../../../shared/modal/modal-reserva-planes/modal-reserva-planes.component';
import { MatDialog } from '@angular/material';

import { ModalPlanesProgramasComponent } from '../../../shared/modal/modal-planes-programas/modal-planes-programas.component'

@Component({
  selector: 'app-box-car',
  templateUrl: './box-car.component.html',
  styleUrls: ['./box-car.component.sass']
})
export class BoxCarComponent implements OnInit {

  @Input() newData: any;

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
    console.log(this.newData)
  }

    // OPEN MODAL LOGIN
    openLogin(data, extra): void{
     
     const diallogRef = this.dialog.open(ModalReservaPlanesComponent, {
       data : {
         extra: extra ? extra : '',
         page: 'home',
         idPlans: data
       }
     });
     diallogRef.afterClosed().subscribe(res => {
       
     })
   }

       // OPEN MODAL
 openModal(data, data2, data3){



  if(data === 'details'){
    const diallogRef = this.dialog.open(ModalPlanesProgramasComponent,  {
      data: {
        message: data,
        show: true,
        page: 'home',
        dataPrograms: data2,
        html: data3
      }
    });
  
    diallogRef.afterClosed().subscribe(result => {
 
  
      if(result){
      
        const vacio = null;
        const vacioA = null;
  
        this.openLogin(vacio, result)
      }
     
    });
  
  }
  
  else if(data === 'comparador'){
    const diallogRef = this.dialog.open(ModalPlanesProgramasComponent,  {
      data: {
        message: data,
        page: 'home',
        html: data2
      }
    },  );
    diallogRef.afterClosed().subscribe(result => {
     
    });
  }
  
  
  
  }

}
