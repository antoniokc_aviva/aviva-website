import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/auth/auth.service'
import {  ActivatedRoute } from '@angular/router';
import { PlanesProgramasService } from '../planes-programas.service';

@Component({
  selector: 'app-planes-programas-details',
  templateUrl: './planes-programas-details.component.html',
  styleUrls: ['./planes-programas-details.component.sass']
})
export class PlanesProgramasDetailsComponent implements OnInit {

  public id;
  public newData: {
    priceLetf: ''
  };
  public slug;
  public stateNav = true;

  constructor(private planesService : PlanesProgramasService, private auth: AuthService, private activeRoute : ActivatedRoute) { }

  ngOnInit() {
    this.auth.stateNav.next(this.stateNav)

    this.activeRoute.params.subscribe(data => {
      this.slug = data.slug; 
    });

    this.planesService.getData(this.slug).subscribe((data: any) => {
      this.newData = data.data;
   
    })


  }

}
