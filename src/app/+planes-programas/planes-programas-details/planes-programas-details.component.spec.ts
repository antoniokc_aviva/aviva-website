import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanesProgramasDetailsComponent } from './planes-programas-details.component';

describe('PlanesProgramasDetailsComponent', () => {
  let component: PlanesProgramasDetailsComponent;
  let fixture: ComponentFixture<PlanesProgramasDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanesProgramasDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanesProgramasDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
