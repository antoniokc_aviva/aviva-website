import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxTabComponent } from './box-tab.component';

describe('BoxTabComponent', () => {
  let component: BoxTabComponent;
  let fixture: ComponentFixture<BoxTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoxTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
