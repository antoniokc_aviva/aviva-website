import { Component, OnInit, Input } from '@angular/core';
import { ModalPlanesProgramasComponent } from '../../../shared/modal/modal-planes-programas/modal-planes-programas.component';
import { ModalReservaPlanesComponent } from '../../../shared/modal/modal-reserva-planes/modal-reserva-planes.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-box-tab',
  templateUrl: './box-tab.component.html',
  styleUrls: ['./box-tab.component.sass']
})
export class BoxTabComponent implements OnInit {

  public show;
  public showModal: boolean = false;

  @Input() newData: any;

  constructor(public dialog: MatDialog,) { }

  ngOnInit() {
    this.show = 0;
    
  }

      // OPEN MODAL
 openModal(data, data2, data3){

  if(data === 'details'){
    const diallogRef = this.dialog.open(ModalPlanesProgramasComponent,  {
      data: {
        message: data,
        show: true,
        page: 'home',
        dataPrograms: data2,
        html: data3
      }
    });
  
    diallogRef.afterClosed().subscribe(result => {
 
  
      if(result){
      
        const vacio = null;
        const vacioA = null;
  
        this.openLogin(vacio, result)
      }
     
    });
  
  }
  
  else if(data === 'comparador'){
    const diallogRef = this.dialog.open(ModalPlanesProgramasComponent,  {
      data: {
        message: data,
        page: 'home',
        html: data2
      }
    },  );
    diallogRef.afterClosed().subscribe(result => {
     
    });
  }
  
  
  
  }

   // OPEN MODAL LOGIN
 openLogin(data, extra): void{
  
 const diallogRef = this.dialog.open(ModalReservaPlanesComponent, {
   data : {
     extra: extra ? extra : '',
     page: 'home',
     idPlans: data
   }
 });
 diallogRef.afterClosed().subscribe(res => {
   
 })
}

showPlan(event){
  const id = +event.currentTarget.attributes.id.nodeValue;
  this.show = id

}

showInfo(data){

}

}
