import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxTabTabComponent } from './box-tab-tab.component';

describe('BoxTabTabComponent', () => {
  let component: BoxTabTabComponent;
  let fixture: ComponentFixture<BoxTabTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoxTabTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxTabTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
