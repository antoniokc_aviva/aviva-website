import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-box-tab-tab',
  templateUrl: './box-tab-tab.component.html',
  styleUrls: ['./box-tab-tab.component.sass']
})
export class BoxTabTabComponent implements OnInit {

  @Input() newData: any;
  public show;

  constructor() { }

  ngOnInit() {
    this.show = 0;
  }

  showPlan(event){
    const id = +event.currentTarget.attributes.id.nodeValue;
    this.show = id

  }
}
