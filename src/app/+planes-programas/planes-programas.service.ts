import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpRequest } from '@angular/common/http';
import { AuthService } from '../shared/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class PlanesProgramasService {


  constructor(public http: HttpClient, private auth: AuthService) { }

  public urlBase = this.auth.urlApiNodos;

  getPlans() {
    return this.http
      .get(this.urlBase + 'plans');
  }

  getData(data){
    return this.http
     .get(this.urlBase  + 'plans/' + data);
  }

  getplanVivaMas(data){
    return this.http
     .get(this.urlBase  + 'plans/' + data);
  }

  getProgramasEmbarazo(){
    return this.http
     .get(this.urlBase  + 'plans/programas-de-embarazo');
  }

  getNiñoSano(){
    return this.http
     .get(this.urlBase  + 'plans/programa-de-nino-sano');
  }

  getChequeoPReventivo(){
    return this.http
     .get(this.urlBase  + 'plans/chequeo-preventivo');
  }
  startReser(data){
    return this.http
     .post(this.urlBase + 'plans/reserve', data);
  }

  //SEND PLANS

  savePlans(data, info){
    return this.http
      .post(this.urlBase + 'plans/'+ data +'/reserve', info)
  }

  savePlansSpecific(data: any, info: any){
    const url = data
    return this.http
      .post(url, info )
  }
}
