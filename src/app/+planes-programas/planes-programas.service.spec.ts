import { TestBed } from '@angular/core/testing';

import { PlanesProgramasService } from './planes-programas.service';

describe('PlanesPaquetesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlanesProgramasService = TestBed.get(PlanesProgramasService);
    expect(service).toBeTruthy();
  });
});
