import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConoceAvivaComponent } from './conoce-aviva.component';

describe('ConoceAvivaComponent', () => {
  let component: ConoceAvivaComponent;
  let fixture: ComponentFixture<ConoceAvivaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConoceAvivaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConoceAvivaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
