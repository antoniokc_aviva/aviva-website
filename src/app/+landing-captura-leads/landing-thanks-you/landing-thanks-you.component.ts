import { Component, OnInit } from '@angular/core';
import { LandingReservasService } from '../landing-captura-leads.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-landing-thanks-you',
  templateUrl: './landing-thanks-you.component.html',
  styleUrls: ['./landing-thanks-you.component.sass']
})
export class LandingThanksYouComponent implements OnInit {

  lat: number = -11.956256;
  lng: number = -77.069612;
  mapType = 'roadmap';
  zoom = 15;

  nameUser = '';

  constructor(private route : Router ,private landingService : LandingReservasService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
  //   this.nameUser = this.landingService.nameUser;
  //  if(this.nameUser === null){
  //    this.route.navigateByUrl('/404')
  //  }
  }

}
