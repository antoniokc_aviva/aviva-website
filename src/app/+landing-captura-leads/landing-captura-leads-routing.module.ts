import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// IMPORT COMPONENT

import { LandingHomeComponent } from './landing-home/landing-home.component';
import { LandingThanksYouComponent } from './landing-thanks-you/landing-thanks-you.component';
import { TerminosYCondicionesComponent } from './terminos-y-condiciones/terminos-y-condiciones.component';


const routes: Routes = [
  {
    path:'',
    component: LandingHomeComponent
  },
  {
    path: 'registro',
    loadChildren: () => import('src/app/+landing-captura-leads/landing-cotizador/landing-cotizador.module').then(m => m.LandingCotizadorModule)
  },
  {
    path: 'thanks-you',
    component: LandingThanksYouComponent
  },
  {
    path: 'terminos-y-condiciones',
    component: TerminosYCondicionesComponent
  }
   // {
  //   path:'',
  //   component: ReservasComponent,
  //   children: [{
  //     path: '', component: ReservasComponentContent
  //   }]
  // },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LandingReservasRoutingModule { }
