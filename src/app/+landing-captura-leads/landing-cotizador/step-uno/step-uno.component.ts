import { Component, OnInit } from '@angular/core';
import { LandingReservasService } from '../../landing-captura-leads.service';
import { Router } from '@angular/router';
import { EspecialidadesDoctoresService } from '../../../+especialidades-y-doctores/especialidades-doctores.service';
@Component({
  selector: 'app-step-uno',
  templateUrl: './step-uno.component.html',
  styleUrls: ['./step-uno.component.sass']
})
export class StepUnoComponent implements OnInit {
  public mailInvalid: boolean = false;
  // NAME
  public name: string = '';
  public nameReadyValidate: boolean = false;
  public nameValidate: boolean;

   // LAST NAME
   public lastName: string = '';
   public lastNameReadyValidate: boolean = false;
   public lastNameValidate: boolean;

    // LAST NAME
    public lastName2: string = '';
    public lastNameReadyValidate2: boolean = false;
    public lastNameValidate2: boolean;

  public loaderSession: boolean = false;

     // NUMBER DOCUMENT
  public phoneNumber: any = '';
  public phoneReadyValidate: boolean = false;
  public phoneValidate: boolean;

    // NUMBER DOCUMENT
    public documentNumber: any = '';
    public documentReadyValidate: boolean = false;
    public documentValidate: boolean;

  // EMAIL
  public email: string = '';
  public emailReadyValidate: boolean = false;
  public emailValidate: boolean;

  // ESPECIALIDAD
  public sexo: any = 'Seleccionar';
  public selectSexo: any;
  public sexoValidate: boolean = false;
  public sexoID;
  public sexoData;

   // EXPRESIONS REGULAR
   public ER_NUM = /^([0-9])*$/;
   public ER_ONLY_NUM: any = /[0-9]/;
   public ER_STR: any = /^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/;
   public ER_STR_MA: any = /[A-Z]/;
   public ER_EMA = /[\w-\.]{1,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
   public checked: any;
   slug;

   public color: any = 'accent';
   public mode: any = 'indeterminate';
   dataSpecialty;
   // DATA OBSERVABLE

   public dataSteps = {
     data1 : true,
     data2 : false
   }

  constructor(private specialtyService : EspecialidadesDoctoresService, public landingService: LandingReservasService, private route : Router) { }

  ngOnInit() {
  
    window.scrollTo(0, 0);
    this.specialtyService.getSpecialty().subscribe((data:any) => {
      this.dataSpecialty = data.data;
    }, (error: any) => {
    })

  }

  selecGender(event){
    this.selectSexo = event.target.selectedOptions[0].textContent;
    this.sexoID = event.target.value;
    if(this.sexo != this.selectSexo){
      this.sexoValidate = true;
    }else{
      this.sexoValidate = false;
    }
  }


  // VALIDATOR STRING
  onkeyValidateString(data:any): void{
    if(this.nameReadyValidate && data === this.name){
      if(data === this.name){
        this.blurValidateString(data);
      }
    }
    else if(this.lastNameReadyValidate && data === this.lastName){
      if(data === this.lastName){
        this.blurValidateString(data);
      }
    }

    else if(this.lastNameReadyValidate2 && data === this.lastName2){
      if(data === this.lastName2){
        this.blurValidateString(data);
      }
    }


    else if(this.phoneReadyValidate && data === this.phoneNumber){
      if(data === this.phoneNumber){
        this.blurValidateString(data);
      }
    }

    else if(this.documentReadyValidate && data === this.documentNumber){
      if(data === this.documentNumber){
        this.blurValidateString(data);
      }
    }

    else if(this.emailReadyValidate && data === this.email){
      if(data === this.email){
        this.blurValidateString(data);
      }
    }
  }

  blurValidateString(data:any): void {
    
   if(data === this.name){
    this.nameReadyValidate = true;
    if( this.validateInput(this.ER_STR, data)){
      this.nameValidate = true;
    }else{
      this.nameValidate = false;
    }
   }

   else if(data === this.lastName){
    this.lastNameReadyValidate = true;
    if( this.validateInput(this.ER_STR, data)){
     this.lastNameValidate = true;
   }else{
     this.lastNameValidate = false;
   }
  }

  else if(data === this.lastName2){
    this.lastNameReadyValidate2 = true;
    if( this.validateInput(this.ER_STR, data)){
     this.lastNameValidate2 = true;
   }else{
     this.lastNameValidate2 = false;
   }
  }

  else if(data === this.phoneNumber){
    this.phoneReadyValidate = true;
    if( this.validateInput(this.ER_NUM, data) && data.length > 1){
     this.phoneValidate = true;
   }else{
     this.phoneValidate = false;
   }
  }

  else if(data === this.documentNumber){
    this.documentReadyValidate = true;
    if( this.validateInput(this.ER_NUM, data)&& data.length == 8){
     this.documentValidate = true;
   }else{
     this.documentValidate = false;
   }
  }
  
   
  else if(data === this.email){
    this.emailReadyValidate = true;
    if( this.validateInput(this.ER_EMA, data)){
     this.emailValidate = true;
   }else{
     this.emailValidate = false;
   }
  }
  }

   // VALIDATE INPUTS EXPRESIONES REGULARES
 validateInput(expresion: any, input: string): boolean{
  if(expresion.test(input)){
    return true;
  }else{
    return false;
  }
}

validateAllInputs(): boolean{
  if(this.nameValidate && this.lastNameValidate && this.lastNameValidate2 && this.phoneValidate && this.sexoValidate && this.checked && this.documentValidate){
    return false;
  }else{
    return true;
  }
}


redirectTo(){

  this.loaderSession = true;

  const dataForm = {
    firstname: this.name,
    lastname: this.lastName,
    dni: this.documentNumber,
    email: this.email,
    phone: this.phoneNumber,
    specialties_data: this.selectSexo
  }


  
  this.landingService.sendForm1(dataForm).subscribe(data => {

    if(data.status === 'success'){
      // this.landingService.dataUsuario = data.data;
      // this.landingService.nameUser = this.name;
      // this.landingService.emailUser = dataForm.email;
      this.route.navigateByUrl(`promo-primera-cita/thanks-you`);
    }
  }, error => {
   
  })


  // this.route.navigateByUrl(`landing-page/${this.slug}/cotizador/resultado`)
}

   // VALIDATE PASSWORD KEYUP && BLUR
   onkeyPass() {

    this.validateInputSim(this.phoneNumber)
    
  } 

// VALIDATE SIMPLE
validateInputSim(input: any){
    if(input.length > 1 && this.validateInput(this.ER_NUM, this.phoneNumber)){
      this.phoneValidate = true;
      this.phoneReadyValidate = true;
    }else{
      this.phoneValidate = false;
    }
}


}
