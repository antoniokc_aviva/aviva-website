import { Component, OnInit } from '@angular/core';
import { LandingReservasService } from '../landing-captura-leads.service';

@Component({
  selector: 'app-landing-cotizador',
  templateUrl: './landing-cotizador.component.html',
  styleUrls: ['./landing-cotizador.component.sass']
})
export class LandingCotizadorComponent implements OnInit {

  dataObs = {
    data1 : '',
    data2 : ''
  };

  constructor(private landingService : LandingReservasService) { }

  ngOnInit() {
    this.landingService._activateBarProgress.subscribe(data => {
      this.dataObs = data
      console.log(this.dataObs)
    })
  }

}
