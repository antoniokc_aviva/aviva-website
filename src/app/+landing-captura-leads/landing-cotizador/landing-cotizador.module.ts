import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StepUnoComponent } from './step-uno/step-uno.component';
import { StepDosComponent } from './step-dos/step-dos.component';
import { LandingCotizadorRoutingModule } from './landing-cotizador-routing.module';
import { LandingCotizadorComponent } from './landing-cotizador.component';
import { FormsModule } from '@angular/forms';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { MatCheckboxModule } from '@angular/material';


@NgModule({
  declarations: [  
  StepUnoComponent,
  StepDosComponent,
  LandingCotizadorComponent
  ],
  imports: [
    CommonModule,
    LandingCotizadorRoutingModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatCheckboxModule
  ]
})
export class LandingCotizadorModule { }
