import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingCotizadorComponent } from './landing-cotizador.component';
import { StepUnoComponent } from './step-uno/step-uno.component';
import { StepDosComponent } from './step-dos/step-dos.component';

const routes: Routes = [
  {
    path: '',
    component: LandingCotizadorComponent,
    children: [{
      path: '', component: StepUnoComponent
      }]
  },
  {
    path: 'resultado',
    component: LandingCotizadorComponent,
    children: [{
      path: '', component: StepDosComponent
      }]
  }
  // {
  //   path:'',
  //   component: ReservasComponent,
  //   children: [{
  //     path: '', component: ReservasComponentContent
  //   }]
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LandingCotizadorRoutingModule { }
