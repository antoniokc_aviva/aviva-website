import { Component, OnInit } from '@angular/core';

import { LandingReservasService } from '../../landing-captura-leads.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-step-dos',
  templateUrl: './step-dos.component.html',
  styleUrls: ['./step-dos.component.sass']
})
export class StepDosComponent implements OnInit {
  public mailInvalid: boolean = false;
  // NAME
  public name: string = '';
  public nameReadyValidate: boolean = false;
  public nameValidate: boolean;

   // LAST NAME
   public lastName: string = '';
   public lastNameReadyValidate: boolean = false;
   public lastNameValidate: boolean;

  public loaderSession: boolean = false;

     // NUMBER DOCUMENT
  public phoneNumber: any = '';
  public phoneReadyValidate: boolean = false;
  public phoneValidate: boolean;

  // EMAIL
  public email: string = '';
  public emailReadyValidate: boolean = false;
  public emailValidate: boolean;

   // EXPRESIONS REGULAR
   public ER_NUM = /^([0-9])*$/;
   public ER_ONLY_NUM: any = /[0-9]/;
   public ER_STR: any = /^[A-Za-z\_\-\.\s\xF1\xD1]+$/;
   public ER_STR_MA: any = /[A-Z]/;
   public ER_EMA = /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;

   public color: any = 'accent';
   public mode: any = 'indeterminate';

   slug;
  dataUser;
  nameUser;
  emailUser = '';
  dataUsuario;

  public dataSteps = {
    data1 : false,
    data2 : true
  }

  constructor(private landingService: LandingReservasService, private route: Router) { }

  ngOnInit() {
    this.landingService._activateBarProgress.next(this.dataSteps);
    this.nameUser = this.landingService.nameUser;
    this.slug = this.landingService.slugLanding;
    this.dataUser = this.landingService.dataLanding;
    this.emailUser = this.landingService.emailUser;
    this.dataUsuario = this.landingService.dataUsuario;
    console.log(this.emailUser)
    console.log(this.landingService.dataLanding);
    if(this.slug === null){
      this.route.navigateByUrl('/404')
    }
  }
// VALIDATOR STRING
onkeyValidateString(data:any): void{


  if(this.phoneReadyValidate && data === this.phoneNumber){
    if(data === this.phoneNumber){
      this.blurValidateString(data);
    }
  }

  else if(this.emailReadyValidate && data === this.email){
    if(data === this.email){
      this.blurValidateString(data);
    }
  }
}

blurValidateString(data:any): void {
  




 if(data === this.phoneNumber){
  this.phoneReadyValidate = true;
  if( this.validateInput(this.ER_NUM, data) && data.length > 8){
   this.phoneValidate = true;
 }else{
   this.phoneValidate = false;
 }
}
 
else if(data === this.email){
  this.emailReadyValidate = true;
  if( this.validateInput(this.ER_EMA, data)){
   this.emailValidate = true;
 }else{
   this.emailValidate = false;
 }
}

}

 // VALIDATE INPUTS EXPRESIONES REGULARES
validateInput(expresion: any, input: string): boolean{
if(expresion.test(input)){
  return true;
}else{
  return false;
}
}

validateAllInputs(): boolean{
if( this.phoneValidate){
  return false;
}else{
  return true;
}
}

sendForm(){

  this.loaderSession = true;
  const informationData = {
    email: this.email,
    telephone: this.phoneNumber
  }
  this.landingService.addInformation(informationData).subscribe(data => {
    if(data.status === 'success'){
      this.route.navigateByUrl(`/landing-page/${this.slug}/thanks-you`);
    }
  }, error => {
    this.loaderSession = false;
  })
}



onkeyPass() {
  console.log('keeyyyy')
  this.validateInputSim(this.phoneNumber)
  
} 

// VALIDATE SIMPLE
validateInputSim(input: any){
  if(input.length > 8 && this.validateInput(this.ER_NUM, this.phoneNumber)){
    this.phoneValidate = true;
    this.phoneReadyValidate = true;
  }else{
    this.phoneValidate = false;
  }
}



}
