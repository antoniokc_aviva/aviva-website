import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute  } from '@angular/router';
import { LandingReservasService } from '../landing-captura-leads.service'

@Component({
  selector: 'app-landing-home',
  templateUrl: './landing-home.component.html',
  styleUrls: ['./landing-home.component.sass']
})
export class LandingHomeComponent implements OnInit {

  public config1 = {
    direction: 'horizontal',
    loop: false,
    spaceBetween: 5,
    cssWidthAndHeight: true,
    slidesPerView: 'auto',
visibilityFullFit: true,
autoResize: false,
  }

  slug: any = '';

  preloader = true;

  public color: any = 'accent';
  public mode: any = 'indeterminate';

  dataPlans;

  widthDocument = document.body.offsetWidth;

  showDesktop: boolean;

  constructor(private route: Router,  private landingReservaService: LandingReservasService) { }

  ngOnInit() {

    if (this.widthDocument > 500) {
      this.showDesktop = true;
    } else {
      this.showDesktop = false;
    }

  }


  redirectTo(): void {
    this.route.navigateByUrl(`promo-primera-cita/registro`);
  }

}
