import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingHomeComponent } from './landing-home/landing-home.component';
import { LandingThanksYouComponent } from './landing-thanks-you/landing-thanks-you.component';
import { LandingReservasRoutingModule } from './landing-captura-leads-routing.module';

import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatCheckboxModule } from '@angular/material';

/* import { AgmCoreModule } from '@agm/core'; */
import { TerminosYCondicionesComponent } from './terminos-y-condiciones/terminos-y-condiciones.component';

export const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {

  direction: 'horizontal',
  slidesPerView: 1,
  loop: false,
  spaceBetween: 8

};

@NgModule({
  declarations: [LandingHomeComponent, LandingThanksYouComponent, TerminosYCondicionesComponent],
  imports: [
    CommonModule,
    LandingReservasRoutingModule,
    SwiperModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    /* AgmCoreModule */
  ],
  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    }
  ]
})
export class LandingCapturaLeadsModule { }
