import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromoDentalPasoUnoComponent } from './promo-dental-paso-uno.component';

describe('PromoDentalPasoUnoComponent', () => {
  let component: PromoDentalPasoUnoComponent;
  let fixture: ComponentFixture<PromoDentalPasoUnoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromoDentalPasoUnoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromoDentalPasoUnoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
