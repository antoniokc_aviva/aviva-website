import { TestBed } from '@angular/core/testing';

import { PromoDentalService } from './promo-dental.service';

describe('PromoDentalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PromoDentalService = TestBed.get(PromoDentalService);
    expect(service).toBeTruthy();
  });
});
