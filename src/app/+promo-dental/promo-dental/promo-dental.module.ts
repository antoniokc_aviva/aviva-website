import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PromoDentalRoutingModule } from './promo-dental-routing.module';
import { PromoDentalPasoUnoComponent } from './promo-dental-paso-uno/promo-dental-paso-uno.component';
import { PromoDentalPasoDosComponent } from './promo-dental-paso-dos/promo-dental-paso-dos.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { MatCheckboxModule } from '@angular/material';
import { PromoDentalTycComponent } from './promo-dental-tyc/promo-dental-tyc.component';


@NgModule({
  declarations: [PromoDentalPasoUnoComponent, PromoDentalPasoDosComponent, PromoDentalTycComponent],
  imports: [
    CommonModule,
    PromoDentalRoutingModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatCheckboxModule
  ]
})
export class PromoDentalModule { }
