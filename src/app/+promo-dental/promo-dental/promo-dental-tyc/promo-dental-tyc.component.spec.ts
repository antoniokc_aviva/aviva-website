import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromoDentalTycComponent } from './promo-dental-tyc.component';

describe('PromoDentalTycComponent', () => {
  let component: PromoDentalTycComponent;
  let fixture: ComponentFixture<PromoDentalTycComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromoDentalTycComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromoDentalTycComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
