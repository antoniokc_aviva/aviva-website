import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromoDentalPasoDosComponent } from './promo-dental-paso-dos.component';

describe('PromoDentalPasoDosComponent', () => {
  let component: PromoDentalPasoDosComponent;
  let fixture: ComponentFixture<PromoDentalPasoDosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromoDentalPasoDosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromoDentalPasoDosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
