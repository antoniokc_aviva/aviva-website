import { Component, OnInit } from '@angular/core';
import { PromoDentalService  } from '../promo-dental.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-promo-dental-paso-dos',
  templateUrl: './promo-dental-paso-dos.component.html',
  styleUrls: ['./promo-dental-paso-dos.component.sass']
})
export class PromoDentalPasoDosComponent implements OnInit {

  name = null;

  constructor(public route: Router ,public promoService: PromoDentalService) { }

  ngOnInit() {
    this.name = this.promoService.nameUser;
    if(this.name === null){
      this.route.navigateByUrl('/promo-dental');
    }
    window.scrollTo(0, 0);
  }

}
