import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PromoDentalPasoUnoComponent } from './promo-dental-paso-uno/promo-dental-paso-uno.component';
import { PromoDentalPasoDosComponent } from './promo-dental-paso-dos/promo-dental-paso-dos.component';
import {  PromoDentalTycComponent } from './promo-dental-tyc/promo-dental-tyc.component';

const routes: Routes = [
  {
    path: '',
    component: PromoDentalPasoUnoComponent
  },
  {
    path: 'gracias',
    component: PromoDentalPasoDosComponent
  },
  {
    path: 'terminos-y-condiciones',
    component: PromoDentalTycComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PromoDentalRoutingModule { }
