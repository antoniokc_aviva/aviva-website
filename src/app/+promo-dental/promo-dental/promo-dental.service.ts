import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PromoDentalService {

  url = environment.urlApiNodos;

  nameUser = null;

  constructor(private http : HttpClient) {

   }

   sendForm(data:Object){
    return this.http.post(this.url + 'captureleads-promo-dental', data);
  }
}
