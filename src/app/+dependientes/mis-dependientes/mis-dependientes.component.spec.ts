import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MisDependientesComponent } from './mis-dependientes.component';

describe('MisDependientesComponent', () => {
  let component: MisDependientesComponent;
  let fixture: ComponentFixture<MisDependientesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MisDependientesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MisDependientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
