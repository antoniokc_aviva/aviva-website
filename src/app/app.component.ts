import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { NavigationEnd, NavigationStart, ResolveEnd, Router } from '@angular/router';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { filter } from 'rxjs/operators';
import { AuthService } from './shared/auth/auth.service';
import { CovidComponent } from './shared/modal/covid/covid.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'aviva';

  constructor(private auth: AuthService, 
              private router: Router, 
              private gtmService: GoogleTagManagerService,
              private dialog: MatDialog ) {

    
      this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((e:any) => {
        /* console.log('okkkkkkkk'); */
        
        const gtmTag = {
          event: 'page',
          pageName: e.url
        };
        this.gtmService.pushTag(gtmTag);
          
      });
    
   
     
  }
  ngOnInit() {
    if (localStorage.getItem('session')) {
      /* console.log('carga data una vez'); */
      /* this.openCovid(); */
    } else {
      this.auth.getSesionPublic();
    }
  }

  openCovid(){
    this.dialog.open(CovidComponent)
  }
}
