import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReservaDoctorRutasRoutingModule } from './reserva-doctor-rutas-routing.module';

import { RegisterComponent } from './register/register.component';
import { FormsModule } from '@angular/forms';
import { SeguroComponent } from './seguro/seguro.component';
import { PagoComponent } from './pago/pago.component';

import { ReservaDoctorComponent } from './reserva-doctor.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { ReservaModule } from '../../+reservas/reserva.module'

import {
  MatCheckboxModule,
  MatRadioModule,
  } from '@angular/material';
import { ValidateCodeComponent } from './validate-code/validate-code.component';


@NgModule({
  declarations: [
    RegisterComponent,
    SeguroComponent,
    PagoComponent,
    ReservaDoctorComponent,
    ValidateCodeComponent
  ],
  imports: [
    CommonModule,
    ReservaDoctorRutasRoutingModule,
    MatProgressSpinnerModule,
    FormsModule,
    MatCheckboxModule,
    MatRadioModule,
    ReservaModule
  ]
})
export class ReservaDoctorRutasModule { }
