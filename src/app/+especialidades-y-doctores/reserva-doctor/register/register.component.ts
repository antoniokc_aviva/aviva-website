import { Component, OnInit } from '@angular/core';
import { ReservasService } from '../../../+reservas/reservas.service';
import { MatDialog } from '@angular/material';

import { ModalComponent  } from '../../../shared/modal/modal/modal.component';
import { RegisterModalComponent  } from '../../../shared/modal/register-modal/register-modal.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {

   
  progressPage = {
    page : 'reserva-doctor',
    state :  'registro'
  }

  constructor(private reservasService : ReservasService, public dialog: MatDialog) { }

  ngOnInit() {
    this.reservasService._progressPage.next(this.progressPage);

  }

  // OPEN MODAL LOGIN
  openLogin(): void{
    const diallogRef = this.dialog.open(ModalComponent, {
      data: 'reserva-doctor'
    });
    diallogRef.afterClosed().subscribe(res => {
    
    })
  }

  // OPEN MODAL REGISTER

  openRegister(): void{
    const diallogRef = this.dialog.open(RegisterModalComponent, {
      data: 'reserva-doctor'
    });
    diallogRef.afterClosed().subscribe(res => {
    
    })
  }
}
