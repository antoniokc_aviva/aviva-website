import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ReservasService } from '../../+reservas/reservas.service';
@Component({
  selector: 'app-reserva-doctor',
  templateUrl: './reserva-doctor.component.html',
  styleUrls: ['./reserva-doctor.component.sass']
})
export class ReservaDoctorComponent implements OnInit {

  avivaCuida: boolean = false;
  page = 'reserva-doctor';
  pageInternas;
  doctors: boolean = false;
  registro: boolean = false;
  seguro: boolean = false;

  constructor(

    private reservasService: ReservasService,
    private cdRef: ChangeDetectorRef,

  ) { }

  ngOnInit() {

    this.reservasService.progressPage.subscribe((page) => {
      this.pageInternas = page.state;
      this.doctors = page.pageDoctor;
      this.registro = page.pageRegistro;
      this.seguro = page.pageSeguro;
      this.page = page.page;
      
      if (!this.cdRef['destroyed']) {
        this.cdRef.detectChanges();
      }
    });
  }
    userNoAutenticate(){
    const session = JSON.parse(localStorage.getItem('session'));
    if(session.role){
      if(session.role === 'user'){
        return false;
      }else{
        return true;
      }
    }
  }

  backLink(){
    window.history.back();
  }

}
