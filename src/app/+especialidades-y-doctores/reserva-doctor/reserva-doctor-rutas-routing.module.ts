import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterComponent } from './register/register.component';
import { SeguroComponent } from './seguro/seguro.component';
import { PagoComponent } from './pago/pago.component';
import { ReservaDoctorComponent } from './reserva-doctor.component'

const routes: Routes = [
  {
    path:'',
    component: ReservaDoctorComponent,
    children: [{
      path: '', component: RegisterComponent
    }]
  },
  {
    path:'financiador',
    component: ReservaDoctorComponent,
    children: [{
      path: '', component: SeguroComponent
    }]
  },
  {
    path:'valida-code',
    component: ReservaDoctorComponent,
    children: [{
      path: '', component: SeguroComponent
    }]
  },
  {
    path:'pago',
    component: ReservaDoctorComponent,
    children: [{
      path: '', component: PagoComponent
    }]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReservaDoctorRutasRoutingModule { }
