import { Component, OnInit } from '@angular/core';
import { ReservasService } from '../../../+reservas/reservas.service';
import { Subject } from 'rxjs';
import { MatDialog } from '@angular/material';
import { alertComponent } from '../../../shared/modal/alert/alert.component';
import { fadeIn } from '../../../shared/animations/animation'
import { ActivatedRoute, Router } from '@angular/router';

declare const window: any;

@Component({
  selector: 'app-pago',
  templateUrl: './pago.component.html',
  styleUrls: ['./pago.component.sass'],
  animations: [fadeIn]
})
export class PagoComponent implements OnInit {

  public color: any = 'warn';
  public mode: any = 'indeterminate';
  public loader: boolean = false;
  public tokenCulqi;

  public price;

  public appoiemendIdd;
  public financiador

  progressPage = {
    page: 'reserva-doctor',
    state: 'pago'
  }

  public messageAlert;

  public culqiApp: any = window.Culqi;
  constructor(public dialog: MatDialog, private reservasService: ReservasService, private router: Router) { }

  public data_opciones = {
    lang: 'es',
    modal: true,
    installments: false,
    style: {
      desctext: '#20668B',
      logo: 'https://raw.githubusercontent.com/akobashikawa/images/master/aviva-logo-240.png'
    }
  }

  ngOnInit() {
    this.financiador = this.reservasService.financiador;
    if (this.reservasService.priceReser != '') {

      this.price = this.reservasService.priceReser;

      this.reservasService._progressPage.next(this.progressPage);

    } else {
      this.router.navigate(['/'])
    }

  }
  backLink() {
    window.history.back();
  }
}
