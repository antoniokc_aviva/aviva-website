import { Component, OnInit } from '@angular/core';
import { ReservasService } from '../../../+reservas/reservas.service';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../../shared/auth/auth.service';
import * as moment from 'moment';
import { Router } from '@angular/router';


@Component({
  selector: 'app-seguro',
  templateUrl: './seguro.component.html',
  styleUrls: ['./seguro.component.sass']
})
export class SeguroComponent implements OnInit {


  progressPage = {
    page: 'reserva-doctor',
    state: 'seguro'
  }

  public user: string;
  public show: boolean;
  public provisionsId: any;

  //SEND SERVICE

  public dataService: any;

  public serviceId: any;
  public doctorId: any;
  public fecha: any;

  public dataPago: any;
  public preloader: boolean;

  public color: any = 'warn';
  public mode: any = 'indeterminate';

  showBoxRadio: boolean = false;

  labelPosition;
  financerInter: any;
  financerExter: any;

  constructor(private router: Router, private reservasService: ReservasService, private auth: AuthService) { }

  ngOnInit() {

    this.reservasService._progressPage.next(this.progressPage);


    if (this.reservasService.dataJson.length === 0) {
      this.router.navigate(['/especialidades-doctores'])
    } else {
      this.dataService = this.reservasService.dataJson
      this.serviceId = this.dataService.service.id;
      this.doctorId = this.dataService.professional.id;
      this.fecha = moment(this.dataService.appointmentDateTime).format('YYYY-MM-DD');
      this.preloader = true;
      this.reservasService.getPlansFinanciador(this.serviceId, this.doctorId, this.fecha).subscribe((data: any) => {

        data.forEach(element => {
          element.price = element.precio[0].total;
          element.trackingId = this.eliminarDiacriticos(element.plan_desc);
          element.trackingId = element.trackingId.split(" ").join("-");
          element.trackingId = element.trackingId.toLowerCase()
        });

        this.dataPago = data;
        this.financerInter = this.dataPago.filter(x => x.siteds === 0);
        this.financerExter = this.dataPago.filter(x => x.siteds === 1);
        this.preloader = false;


      }, (error: any) => {

      })
    }
  }

  changeState() {
    if (this.showBoxRadio) {
      this.showBoxRadio = false;
    } else {
      this.showBoxRadio = true;
    }
  }

  isUser() {
    if (this.auth.isUser() === 'user') {
      this.user = this.auth.User();
      this.show = true;
      return true
    } else {
      this.user = '';
      this.show = false;
      return false
    }
  }

  select(season, seasons, data, data2) {

    this.reservasService.dataPlansClienteId = data2;
    this.reservasService.dataPlansCliente = data;
    this.reservasService.priceReser = seasons;
    this.reservasService.financiador = season.Financiador;
    this.router.navigateByUrl('especialidades-doctores/reserva-doctor/pago');
  }
  eliminarDiacriticos(texto) {
    return texto.normalize('NFD').replace(/[\u0300-\u036f]/g, "");
  }

  backLink() {
    window.history.back();
  }

}
