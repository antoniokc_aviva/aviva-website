import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservaDoctorComponent } from './reserva-doctor.component';

describe('ReservaDoctorComponent', () => {
  let component: ReservaDoctorComponent;
  let fixture: ComponentFixture<ReservaDoctorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservaDoctorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservaDoctorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
