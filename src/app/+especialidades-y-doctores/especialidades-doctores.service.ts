import { Injectable } from '@angular/core';
import { AuthService } from '../shared/auth/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EspecialidadesDoctoresService {
  

  constructor(public Authservice : AuthService, public http: HttpClient) { }
  urlBase = this.Authservice.getUrlBase();
  urlBaseNodos = this.Authservice.getUrlBaseNodos();

  getProfesionals(){
    return this.http
      .get(this.urlBase + 'api/v2/ebooking/fmt-centers/1/services/all/professionals')
  }

  getSpecialty(){
    return this.http
      .get(this.urlBaseNodos + 'specialties')
  }

  getDataSpecialty(data: any){
    return this.http  
      .get(this.urlBaseNodos + 'specialties/' + data);
  }

  getInfoPlans(data: any): any {
    return this.http
      .get(data)
  }

  getDoctorsSpecialty(params: any) {
    return this.http
      .get(this.urlBase + 'api/v2/ebooking/fmt-centers/1/services/' + params + '/professionals' );
  }

  getDoctorsDispo(specialty: any, profesional: any, date1: any,  date2: any) {
    return this.http
      .get(this.urlBase + 'api/v2/ebooking/fmt-centers/1/services/' + specialty + '/professionals/' + profesional +  '/availables?from_date=' + date1 + '&to_date=' + date2 );
  }

  getAllDoctorsSpecialty(doctors, desde, hasta){
    return this.http
        .get(this.urlBase + `api/v2/ebooking/fmt-centers/1/services/${doctors}/professionals/all/availables?from_date=${desde}&to_date=${hasta}`);
  }
}
