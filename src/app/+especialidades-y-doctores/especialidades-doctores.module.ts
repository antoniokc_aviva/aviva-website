import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EspecialidadesDoctoresRoutingModule } from './especialidades-doctores-routing.module';
import { PlanesProgramasModule } from '../+planes-programas/planes-programas.module';

import { CuraCuidaComponent } from './cura-cuida/cura-cuida.component';
import { EspecialidadesDetailsComponent } from './especialidades-details/especialidades-details.component';
import { EspecialidadesYDoctoresComponent } from './especialidades-y-doctores/especialidades-y-doctores.component';
import { EspecialidadesYDoctoresComponents } from './especialidades-y-doctoress/especialidades-y-doctores.component';
import { ReservarCitaDoctorComponent } from './reservar-cita-doctor/reservar-cita-doctor.component';

import { FormsModule } from '@angular/forms';

import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

import { SwiperModule } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';


import { ReservaModule } from '../+reservas/reserva.module';
import { TabCuraCuidaComponent } from './especialidades-details/tab-cura-cuida/tab-cura-cuida.component';
import { BoxCuraComponent } from './especialidades-details/box-cura/box-cura.component';
import { BoxCuidaComponent } from './especialidades-details/box-cuida/box-cuida.component';
import { DoctorsListModule } from './doctors-list/doctors-list.module'
import { MatExpansionModule } from '@angular/material';

export const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  
  direction: 'horizontal',
  slidesPerView: 3,
  loop: true,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  pagination: {
    clickable: true,
    el: '.swiper-pagination',
    type: 'bullets',
    hideOnClick: false
  },
  spaceBetween:50, 
  breakpoints: {

    // when window width is <= 640px
    768: {
      slidesPerView: 2,
      spaceBetween:10,
      
    }
  }
  
};



@NgModule({
  declarations: [
    CuraCuidaComponent,
    EspecialidadesDetailsComponent,
    EspecialidadesYDoctoresComponent,
    EspecialidadesYDoctoresComponents,
    ReservarCitaDoctorComponent,
    TabCuraCuidaComponent,
    BoxCuraComponent,
    BoxCuidaComponent
  ],
  imports: [
    CommonModule,
    EspecialidadesDoctoresRoutingModule,
    PlanesProgramasModule,
    SwiperModule,
    MatProgressSpinnerModule,
    MatExpansionModule,
    FormsModule,
    ReservaModule,
    DoctorsListModule 
  ],
  exports: [
    CuraCuidaComponent,
    EspecialidadesDetailsComponent,
    EspecialidadesYDoctoresComponent,
    EspecialidadesYDoctoresComponents,
    ReservarCitaDoctorComponent,
  ]
})
export class EspecialidadesDoctoresModule { }
