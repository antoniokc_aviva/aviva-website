import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EspecialidadesYDoctoresComponent } from './especialidades-y-doctores.component';

describe('EspecialidadesYDoctoresComponent', () => {
  let component: EspecialidadesYDoctoresComponent;
  let fixture: ComponentFixture<EspecialidadesYDoctoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EspecialidadesYDoctoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EspecialidadesYDoctoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
