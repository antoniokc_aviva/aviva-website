import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EspecialidadesDoctoresService } from '../especialidades-doctores.service';
import { AuthService } from '../../shared/auth/auth.service';
import { BreakpointObserver } from '@angular/cdk/layout';


@Component({
  selector: 'app-especialidades-details',
  templateUrl: './especialidades-details.component.html'
})
export class EspecialidadesDetailsComponent implements OnInit, OnDestroy {

  public dataLimit: any = [];
  public dataLimitCura: any = [];
  public urlBase = '';
  public moreInfo: boolean = false;
  public moreInfo2: boolean = false;
  public urlBaseAlter = ''

  public id: any = '';
  public preloader;

  // GET DOCTORS

  public dataDoctorsCura: any;
  public dataDoctorsCuida: any;

  public contentCura = [

    {
      fullName: "",
      id: '',
      info: '',
      almaMater: "",
      centers: {
        services : {
          profesionals: ''
        }
      },
      cmp: "",
      fullname: "",
      imageUrl: "",
      rne: "",
      services: [{
        id: '',
        professionals : ''
      }],
      service:{
        id: ''
      }

    }
    
  ]
  public contentCuida: any = [
    {
      fullName: "",
      id: '',
      info: '',
      almaMater: "",
      centers: {
        services : {
          profesionals: ''
        }
      },
      cmp: "",
      fullname: "",
      imageUrl: "",
      rne: "",
      services: [{
        id: '',
        professionals : ''
      }],
      service:{
        id: ''
      }

    }

  ]
  public showImg: boolean = false;
  public showCura: boolean;
  public showCuida: boolean;

  public active: boolean = true;

  public color: any = 'accent';
  public mode: any = 'indeterminate';
  public dataServices = {
   
  }
    
  public showPage: any;

  public showButton: boolean = false;
  public showButtonCura: boolean = false;

  // DATA PLANS
  public dataPlans: any = {
    planServices : []
  };

  public config1 = {
    direction: 'horizontal',
    slidesPerView: 3,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    pagination: {
      clickable: true,
      el: '.swiper-pagination-top',
      type: 'bullets',
      hideOnClick: false
    },
    spaceBetween: 50,
    breakpoints: {


      768: {
        slidesPerView: 2,
        spaceBetween: 5,

      }
    }
  }

  stateNav = {
    state : true,
    page : 'especialidades'

  };

  
  isDesktop: boolean;
  
  IMAGES = {
    ONE: {
      width: '375',
      height: '420'
    },
    TWO: {
      width: '170',
      height: '163'
    }
  }

  constructor(
    private breakpointObserver: BreakpointObserver,
    private auth: AuthService,private specialidadesServices: EspecialidadesDoctoresService ,private router: Router, private activate: ActivatedRoute) { }

  ngOnInit() {

    this.auth.stateNav2.next(this.stateNav)
    this.urlBase = this.auth.getUrlBase();
    this.urlBaseAlter = this.auth.urlBaseAlter;

    this.showPage = true;

    document.querySelectorAll('.planes-programas-nav')[0].classList.add('footer-gris')
    this.activate.params.subscribe((data: any) => {
      this.id = +data.id;
      this.updateInformation();

    })

    this.breakpointObserver
      .observe(['(min-width: 1200px)'])
      .subscribe((result) => this.responsive(result.matches));
     
  }
  responsive(matches: boolean): void {
    this.IMAGES.ONE= {
      width: matches ? '1920':'375',
      height: matches ? '709':'420',
    }

    this.IMAGES.TWO= {
      width: matches ? '336':'170',
      height: matches ? '323':'163',
    }
  }

  ngOnDestroy(){
    const desactiveNav = false;
    this.auth.stateNav2.next(desactiveNav)
    document.querySelectorAll('.planes-programas-nav')[0].classList.remove('footer-gris')
  }



  changeState(){
    const desactiveNav = false;
    this.auth.stateNav2.next(desactiveNav)
    this.showPage = false;
  }

  getDataDoctors(data){
    
    this.specialidadesServices.getDoctorsSpecialty(data).subscribe((data: any) => {
     return data
    })
  }


  updateInformation(){
    this.dataServices = {}
    this.dataLimit = [];
    this.dataLimitCura = []
    this.moreInfo = false;
    this.moreInfo2= false;
    this.preloader = true;
    this.dataPlans.planServices = []
    this.specialidadesServices.getDataSpecialty(this.id).subscribe((data: any) => {
      console.log(data);
      this.dataServices = data.data;
      this.dataDoctorsCuida = data.data.cuidaId;
      this.dataDoctorsCura = data.data.curaId;


      if(data.data.links.length > 0){

        const linksPlans =  data.data.links[0].href;
  
        this.specialidadesServices.getInfoPlans(linksPlans).subscribe((data: any) => {
          this.dataPlans = data.data;
          /* console.log(this.dataPlans) */
     
        })
      }
      this.preloader = false;



      
    })
  }



  scrollToElement(element): void {

    let top = document.querySelectorAll(element)[0].getBoundingClientRect().height;

    window.scrollTo( {
      top: top,
      left: 0,
      behavior: 'smooth'
    });

  }

}
