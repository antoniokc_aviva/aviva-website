import { Component, OnInit, Input } from '@angular/core';
import { EspecialidadesDoctoresService } from '../../especialidades-doctores.service';
import { AuthService } from '../../../shared/auth/auth.service';

@Component({
  selector: 'app-box-cura',
  templateUrl: './box-cura.component.html',
  styleUrls: ['./box-cura.component.sass']
})
export class BoxCuraComponent implements OnInit {

  @Input() dataDoctorsCura;
  contentCura;
  dataLimitCura;
  showButtonCura;
  moreInfo2;
  urlBaseAlter;

  constructor(private auth: AuthService,private specialidadesServices: EspecialidadesDoctoresService) { }

  ngOnInit() {

   
    console.log(this.dataDoctorsCura)
    this.urlBaseAlter = this.auth.urlBaseAlter;
      this.specialidadesServices.getDoctorsSpecialty(this.dataDoctorsCura).subscribe((data: any) => {

        this.contentCura = data.centers[0].services[0].professionals;

        console.log(this.contentCura)


        if(this.contentCura.length > 6){
          this.dataLimitCura = this.contentCura.slice(0,6);
          this.showButtonCura = true;

        }else{
          this.dataLimitCura = this.contentCura;
          this.showButtonCura = false;
        }
     
       })
  }

  changeTable(data){
  
      this.dataLimitCura = this.contentCura
      this.moreInfo2 = !this.moreInfo2; 
    
  }

  changeTable2(data){
   
      this.dataLimitCura = this.contentCura.slice(0,6);
      this.moreInfo2 = !this.moreInfo2;
    
  }

}
