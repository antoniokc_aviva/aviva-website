import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxCuidaComponent } from './box-cuida.component';

describe('BoxCuidaComponent', () => {
  let component: BoxCuidaComponent;
  let fixture: ComponentFixture<BoxCuidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoxCuidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxCuidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
