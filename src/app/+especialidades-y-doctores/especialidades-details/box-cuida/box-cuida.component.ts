import { Component, OnInit, Input } from '@angular/core';
import { EspecialidadesDoctoresService } from '../../especialidades-doctores.service';
import { AuthService } from '../../../shared/auth/auth.service';

@Component({
  selector: 'app-box-cuida',
  templateUrl: './box-cuida.component.html',
  styleUrls: ['./box-cuida.component.sass']
})
export class BoxCuidaComponent implements OnInit {

  contentCuida;
  dataLimit;
  showButton;
  moreInfo2;
  urlBaseAlter;

  @Input() dataDoctorsCuida;

  constructor(private auth: AuthService,private specialidadesServices: EspecialidadesDoctoresService) { }

  ngOnInit() {

    this.urlBaseAlter = this.auth.urlBaseAlter;
      this.specialidadesServices.getDoctorsSpecialty(this.dataDoctorsCuida).subscribe((data: any) => {
 
        this.contentCuida = data.centers[0].services[0].professionals
        if(this.contentCuida.length > 6){
          this.dataLimit = this.contentCuida.slice(0,6);
          this.showButton = true;

        }else{
          this.dataLimit = this.contentCuida;
          this.showButton = false;
        }
      
        
       })
  
    
   
  }

  changeTable(){
    console.log('dassdas')
    console.log(this.contentCuida)
      this.dataLimit = this.contentCuida

      this.moreInfo2 = !this.moreInfo2;
  
  }

  changeTable2(){
  
      this.dataLimit = this.contentCuida.slice(0,6);
      this.moreInfo2 = !this.moreInfo2;
   
  }

}
