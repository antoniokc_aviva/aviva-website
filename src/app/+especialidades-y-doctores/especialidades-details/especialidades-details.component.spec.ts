import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EspecialidadesDetailsComponent } from './especialidades-details.component';

describe('EspecialidadesDetailsComponent', () => {
  let component: EspecialidadesDetailsComponent;
  let fixture: ComponentFixture<EspecialidadesDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EspecialidadesDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EspecialidadesDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
