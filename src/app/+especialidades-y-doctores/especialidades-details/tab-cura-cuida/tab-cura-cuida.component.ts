import { Component, OnInit, Input } from '@angular/core';
import { EspecialidadesDoctoresService } from '../../especialidades-doctores.service';
import { AuthService } from '../../../shared/auth/auth.service';

@Component({
  selector: 'app-tab-cura-cuida',
  templateUrl: './tab-cura-cuida.component.html',
  styleUrls: ['./tab-cura-cuida.component.sass']
})
export class TabCuraCuidaComponent implements OnInit {
  showCuida;

  @Input() dataDoctorsCuida;
  @Input() dataDoctorsCura;
  contentCuida;
  dataLimit;
  showButton;
  contentCura;
  dataLimitCura;
  showButtonCura;
  showPage;
  active : boolean = true;
  moreInfo;
  moreInfo2;
  urlBaseAlter;
  constructor(private auth: AuthService, private specialidadesServices: EspecialidadesDoctoresService) { }

  ngOnInit() {

    this.showPage = true;
    this.urlBaseAlter = this.auth.urlBaseAlter;
    console.log(this.dataDoctorsCuida, this.dataDoctorsCura)
      this.specialidadesServices.getDoctorsSpecialty(this.dataDoctorsCuida).subscribe((data: any) => {
 
        this.contentCuida = data.centers[0].services[0].professionals
        if(this.contentCuida.length > 6){
          this.dataLimit = this.contentCuida.slice(0,6);
          this.showButton = true;

        }else{
          this.dataLimit = this.contentCuida;
          this.showButton = false;
        }
      
        
       })
  
    
      this.specialidadesServices.getDoctorsSpecialty(this.dataDoctorsCura).subscribe((data: any) => {

        this.contentCura = data.centers[0].services[0].professionals;


        if(this.contentCura.length > 6){
          this.dataLimitCura = this.contentCura.slice(0,6);
          this.showButtonCura = true;

        }else{
          this.dataLimitCura = this.contentCura;
          this.showButtonCura = false;
        }
     
       })
    

  
  }


  changeTabDoctor(){

    this.active = !this.active
   
  }

  changeTable(data){
    if(data === 'cuida'){
      this.dataLimit = this.contentCuida

      this.moreInfo = !this.moreInfo;
    }else{
      this.dataLimitCura = this.contentCura
      this.moreInfo2 = !this.moreInfo2; 
    }
  }

  changeTable2(data){
    if(data === 'cuida'){
      this.dataLimit = this.contentCuida.slice(0,6);
      this.moreInfo = !this.moreInfo;
    }else{
      this.dataLimitCura = this.contentCura.slice(0,6);
      this.moreInfo2 = !this.moreInfo2;
    }
  }

}
