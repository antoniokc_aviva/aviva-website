import { TestBed } from '@angular/core/testing';

import { EspecialidadesDoctoresService } from './especialidades-doctores.service';

describe('EspecialidadesDoctoresService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EspecialidadesDoctoresService = TestBed.get(EspecialidadesDoctoresService);
    expect(service).toBeTruthy();
  });
});
