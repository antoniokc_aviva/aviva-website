import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions, Router } from '@angular/router';
import { CuraCuidaComponent } from './cura-cuida/cura-cuida.component';
import { EspecialidadesDetailsComponent } from './especialidades-details/especialidades-details.component';
import { EspecialidadesYDoctoresComponent } from './especialidades-y-doctores/especialidades-y-doctores.component';
import { EspecialidadesYDoctoresComponents } from './especialidades-y-doctoress/especialidades-y-doctores.component';
import { ReservarCitaDoctorComponent } from './reservar-cita-doctor/reservar-cita-doctor.component';

import { ReservaDoctorComponent } from './reserva-doctor/reserva-doctor.component';
import { ReservaModule } from '../+reservas/reserva.module';
import { DoctoresComponent } from '../+reservas/doctores/doctores.component';

const routes: Routes = [
  {
    path:'',
    component: EspecialidadesYDoctoresComponent,
    children: [{
      path: '', component: EspecialidadesYDoctoresComponents
    }]
  },
  {
    path:'especialidades-details/:id/:slug',
    component: EspecialidadesYDoctoresComponent,
    children: [{
      path: '', component: EspecialidadesDetailsComponent
    }]
  },
  {
    path:'details-doctors/:especialidad/:id',
    component: EspecialidadesYDoctoresComponent,
    children: [{
      path: '', component: ReservarCitaDoctorComponent
    }]
  },
  {
    path:'reservas/avivacura/selecciona-doctor/:id/:especialidad',
    component: ReservaModule,
    children: [{
      path: '', component: DoctoresComponent
    }]
  },
  {
    path:'reserva-doctor',
    component: EspecialidadesYDoctoresComponent,
    loadChildren: () => import('src/app/+especialidades-y-doctores/reserva-doctor/reserva-doctor-rutas.module').then(m => m.ReservaDoctorRutasModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EspecialidadesDoctoresRoutingModule { }
