import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservarCitaDoctorComponent } from './reservar-cita-doctor.component';

describe('ReservarCitaDoctorComponent', () => {
  let component: ReservarCitaDoctorComponent;
  let fixture: ComponentFixture<ReservarCitaDoctorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservarCitaDoctorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservarCitaDoctorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
