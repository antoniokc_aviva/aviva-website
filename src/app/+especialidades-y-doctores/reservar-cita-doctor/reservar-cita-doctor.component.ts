import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EspecialidadesDoctoresService } from '../especialidades-doctores.service';
import * as moment from 'moment';
import 'moment/locale/es'
import { ReservasService } from '../../+reservas/reservas.service';
import { AuthService } from '../../shared/auth/auth.service';
import { ModalDetailsDoctorsComponent } from 'src/app/shared/modal/modal-details-doctors/modal-details-doctors.component';
import { MatDialog } from '@angular/material';
import { RecipesService } from '../../+recipes/recipes.service';

@Component({
  selector: 'app-reservar-cita-doctor',
  templateUrl: './reservar-cita-doctor.component.html',
  styleUrls: ['./reservar-cita-doctor.component.scss']
})
export class ReservarCitaDoctorComponent implements OnInit {

  public urlBaseAlter: any = '';

  public preloader: boolean;
  public preloaderExtraDates: boolean = false;

  public idDoctors: any = '';
  public specialty: any = '';
  // INFORMATION TO SERVICE
  public dateFirst = moment().format('YYYY-MM-DD');
  public dateSecond = moment().add(6, 'days').format('YYYY-MM-DD');
  public idBox;
  stateShoww: boolean;

  boxID: any = null;
  boxCaID: any = null;

  public color: any = 'accent';
  public mode: any = 'indeterminate';
  provisionsData;
  public otherDoctors;
  public _doctorsSpecialty;
  public doctorsSpecialty = [];
  public doctorData;
  datesCalendar: any;

  public dataDoctors ;

  constructor(private auth : AuthService,private routes: Router,private reservasService: ReservasService ,private activateRoute : ActivatedRoute, private specialidadesServices: EspecialidadesDoctoresService, public dialog: MatDialog, public recipeSrv: RecipesService) { }

  ngOnInit() {
    /* console.log('doctores:', this.dataDoctors ); */
    this.urlBaseAlter = this.auth.getUrlBaseAlter();
    this.preloader = true;
  
    this.activateRoute.params.subscribe((data: any) => {
      console.log(data);
      this.idDoctors = data.id;
      this.specialty = data.especialidad;
    })
 
    this.getAllDoctorsSpecialty();

    if(this.idDoctors !== ''){
      this.specialidadesServices.getDoctorsDispo(this.specialty, this.idDoctors, this.dateFirst, this.dateSecond).subscribe((data: any) => {
        this.provisionsData = data.centers[0].services[0].provision;
        console.log('data que me llega a doctores details:',data);
        console.log('data que llega:', data);

        // RESET DATA
        const docts = data.centers[0].services[0].professionals
      
        docts.forEach(element => {
          const fech = element.availables;

            fech.forEach(dat => {
              dat.hours.hour = dat.hours.map((element: any) => {
                return element.hour.slice(0,5);
              });
              dat.newFormatDay = moment(dat.date).locale('es').format('DD');
              dat.date = moment(dat.date).locale('es').format('dddd').slice(0,3);
            });
            this.getDataDoctor();
        });

        this.dataDoctors = docts[0];
        console.log(this.dataDoctors);
   
        // RESET DATA
        this.preloader = false;
        this.preloaderExtraDates = true;
      })
    }

  }


  showBox(index1, index2){
    this.idBox = index1;
  }

  redirectTo(info, provisionsID, items) {
  /* console.log('aa', info, provisionsID, items);  */
    this.reservasService.provisionsId = provisionsID[0];
    const listjson = info;
    const listjson2 = this.dataDoctors;

    const newJson = JSON.parse(listjson);
    /* console.log('newJson', newJson); */
    this.reservasService.dataJson = listjson;
    this.reservasService.dateCita = moment(newJson.appointmentDateTime).locale('es').format('LLLL');
    /* newJson.provisions = [this.provisionsData]; */


    this.reservasService.dataJson = newJson;
    const session = JSON.parse(localStorage.getItem('session'));
    if (session.role === 'user') {
      this.routes.navigateByUrl('reservas/avivacura/seguro');
    } else {
      this.routes.navigateByUrl('reservas/avivacura/registro');
    }
  }

  redirectToa(info, index, doctor, provisionsID) {
    /* console.log('ab', info, index, doctor, provisionsID); */
    this.reservasService.provisionsId = provisionsID[0];
    const listjson = this.doctorsSpecialty[doctor].availables[index].hours[info].listjson;
    this.reservasService.dataJson = listjson;
    console.log(listjson);
    const listjson2 = this.dataDoctors;

    const newJson = JSON.parse(listjson);
    /* console.log('newJson', newJson); */
    this.reservasService.dateCita = moment(newJson.appointmentDateTime).locale('es').format('LLLL');
    /* newJson.provisions = [this.provisionsData]; */


    this.reservasService.dataJson = newJson;
    const session = JSON.parse(localStorage.getItem('session'));
    if (session.role === 'user') {
      this.routes.navigateByUrl('reservas/avivacura/seguro');
    } else {
      this.routes.navigateByUrl('reservas/avivacura/registro');
    }
  }

  backLink(){
    window.history.back();
  }
  
  getAllDoctorsSpecialty(){
    this.specialidadesServices.getAllDoctorsSpecialty(this.specialty, this.dateFirst, this.dateSecond).subscribe((data:any) =>{
      console.log('todos los especialistas:',data);
      this.otherDoctors = data.centers[0].services[0].professionals;
      /* console.log(this.otherDoctors); */
      this._doctorsSpecialty = this.otherDoctors.filter( x => x.availables.length > 0);
     /*  console.log('los doctores que son de la misma especialidad y tienen horas disponibles:',this._doctorsSpecialty); */

      this._doctorsSpecialty.forEach(element => {
        const fech = element.availables;
        this.datesCalendar = fech;

        fech.forEach(dat => {
          dat.hours.hour = dat.hours.map((element: any) => {
            return element.hour.slice(0, 5);
          });
          dat.newFormatDay = moment(dat.date).locale('es').format('DD');
          dat.date = moment(dat.date).locale('es').format('dddd').slice(0, 3);
        });
      })
      this.preloaderExtraDates = false;
      this.doctorsSpecialty = this._doctorsSpecialty;
      console.log('doctores disponibles:',this.doctorsSpecialty);
  })
}


  openLogin(data){
    const diallogRef = this.dialog.open(ModalDetailsDoctorsComponent, {
      data: {
        page: 'aviva-cura',
        infoDetails: data
      },
      width: 'auto'
    });
    diallogRef.afterClosed().subscribe(res => {

    })
  }

  stateShow(item: any, index) {
    console.log(item,index);
    this.boxID = item;
    this.boxCaID = index;
  }

  getDataDoctor(){
    this.recipeSrv.getDatesDoctor(this.idDoctors).subscribe((data:any) => {
      if(data){
        this.doctorData = data.data;
      }else{
        this.doctorData = this.doctorData.length > 0;
      }
      /* console.log(this.doctorData); */
    }, err => {
      this.doctorData = [];
    })
  }
}
