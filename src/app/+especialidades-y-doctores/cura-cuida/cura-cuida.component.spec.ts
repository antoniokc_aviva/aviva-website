import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CuraCuidaComponent } from './cura-cuida.component';

describe('CuraCuidaComponent', () => {
  let component: CuraCuidaComponent;
  let fixture: ComponentFixture<CuraCuidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CuraCuidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CuraCuidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
