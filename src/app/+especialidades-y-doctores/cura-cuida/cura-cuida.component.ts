import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-cura-cuida',
  templateUrl: './cura-cuida.component.html',
  styleUrls: ['./cura-cuida.component.sass']
})
export class CuraCuidaComponent implements OnInit {

  @Input() data: any; 

  constructor() { }

  ngOnInit() {
  }

  backLink(){
    window.history.back();
  }

}
