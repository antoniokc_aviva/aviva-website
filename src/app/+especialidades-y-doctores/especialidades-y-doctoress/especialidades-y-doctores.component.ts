import { Component, OnInit } from '@angular/core';
import { EspecialidadesDoctoresService } from '../especialidades-doctores.service';

@Component({
  selector: 'app-especialidades-y-doctores',
  templateUrl: './especialidades-y-doctores.component.html'
})
export class EspecialidadesYDoctoresComponents implements OnInit {

  public hideBoxSpecialtyDoctors: boolean = false;
  public selectLetter = 'Todos (A-Z)';
  public showSpecialty: boolean = true;
  public orderBox = 'Orden alfabetico';
  public showAll: boolean = false;
  public letrasAbc = ['A','B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'Ñ', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

  // SERVICES
  public dataSpecialty;
  public doctorData;

  public preloader;

  // FILTERS DOCTORS

  public orderInfo = {};
  public orderLetrasMain;

  public color: any = 'accent';
  public mode: any = 'indeterminate';



  constructor(private specialtyService : EspecialidadesDoctoresService) { }

  ngOnInit() {

    this.preloader = true;
    
    this.selectLetter = 'Todos (A-Z)';
    this.orderBox = 'Orden alfabetico';
    // this.specialtyService.getProfesionals().subscribe((data:any) => {
     
    //   console.log(data)
    //     data.forEach(element => {
          
    //       element.letterMain = element.fullname.charAt();

    //       const date = data.filter((elements:any) => elements.letterMain === element.letterMain);

    //       this.orderInfo[element.letterMain] = []
    //       this.orderInfo[element.letterMain].push(date)
  
    //     });
    //     this.orderLetrasMain = Object.keys(this.orderInfo).sort();
    //     console.log(this.orderLetrasMain)
    //     console.log(this.orderInfo)

    // }, (error: any) => {
    //   console.log(error)
    // })

    this.specialtyService.getSpecialty().subscribe((data:any) => {
      this.dataSpecialty = data.data;
      this.preloader = false;
    }, (error: any) => {
      console.log(error)
    })
  }

  changeState(data){
    if(data === 'especialidades'){
      this.showSpecialty = true;
    }else{
      this.showSpecialty = false;
    }
  }

  orderBoxSelect(){
    if(this.orderBox === 'especialidades'){
      this.showAll = true;
    }else {
      this.showAll = false;
    }
  }

  getData(data){

  }
  selectLetterM(){
 
  }
  

}
