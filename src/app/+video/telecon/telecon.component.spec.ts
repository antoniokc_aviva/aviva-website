import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeleconComponent } from './telecon.component';

describe('TeleconComponent', () => {
  let component: TeleconComponent;
  let fixture: ComponentFixture<TeleconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeleconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeleconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
