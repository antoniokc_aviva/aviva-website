import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { NgxAgoraService, Stream, AgoraClient, ClientEvent, StreamEvent } from 'ngx-agora';

@Component({
  selector: 'app-telecon',
  templateUrl: './telecon.component.html',
  styleUrls: ['./telecon.component.scss']
})
export class TeleconComponent implements OnInit {
  topVideoFrame = 'partner-video';
  userId: string;
  partnerId: string;
  myEl: HTMLMediaElement;
  partnerEl: HTMLMediaElement;
  hora;
  title = 'angular-video';

  public localCallId;
  remoteCalls: string[] = [];
  public channel;
  public token;

  public client: AgoraClient;
  public localStream: Stream;
  public uid;
  public dates;
  public nombreDoctor;
  public idDoctor;
  public time;

  public dataPermissions;

  constructor(public router: Router,
    public routes: ActivatedRoute,
    public ngxAgoraService: NgxAgoraService) { }

  ngOnInit() {
    this.crhono();
    const data = this.routes.snapshot.paramMap.get('data');
    this.dataPermissions = JSON.parse(data);
    console.log('dataPermissions:', this.dataPermissions);
    if (this.dataPermissions) {
      this.channel = this.dataPermissions.channel;
      this.token = this.dataPermissions.token;
      this.localCallId = this.dataPermissions.patientId.toString();
      this.uid = this.dataPermissions.uid;
      this.initVideo();
    }

  }

  initVideo() {
    this.client = this.ngxAgoraService.createClient({ mode: 'rtc', codec: 'h264' });
    this.assignClientHandlers();

    this.localStream = this.ngxAgoraService.createStream({ streamID: this.uid, audio: true, video: true, screen: false });
    this.assignLocalStreamHandlers();
    this.initLocalStream(() => this.join(uid => this.publish(), error => console.error(error)));
    this.localStream.setVideoProfile('720p_3');
  }

  crhono() {
    setInterval(() => {
      this.updateTime();
    }, 1000)
  }

  updateTime() {
    this.time = moment().format('hh:mm:ss');
  }

  join(onSuccess?: (uid: number | string) => void, onFailure?: (error: Error) => void): void {
    this.client.join(this.token, this.channel, this.uid, onSuccess, onFailure);
  }

  /**
   * Attempts to upload the created local A/V stream to a joined chat room.
   */
  publish(): void {
    this.client.publish(this.localStream, err => console.log('Publish local stream error: ' + err));
  }

  private assignLocalStreamHandlers(): void {
    this.localStream.on(StreamEvent.MediaAccessAllowed, () => {
      console.log('accessAllowed');
    });

    // The user has denied access to the camera and mic.
    this.localStream.on(StreamEvent.MediaAccessDenied, () => {
      console.log('accessDenied');
    });
  }

  private initLocalStream(onSuccess?: () => any): void {
    this.localStream.init(
      () => {
        // The user has granted access to the camera and mic.
        console.log(this.localCallId);
        this.localStream.play(this.localCallId);

        if (onSuccess) {
          onSuccess();
        }
      },
      err => console.error('getUserMedia failed', err)
    );
  }

  private assignClientHandlers(): void {
    this.client.on(ClientEvent.LocalStreamPublished, evt => {
      console.log('Publish local stream successfully');
    });

    this.client.on(ClientEvent.Error, error => {
      console.log('Got error msg:', error.reason);
      if (error.reason === 'DYNAMIC_KEY_TIMEOUT') {
        this.client.renewChannelKey(
          '',
          () => console.log('Renewed the channel key successfully.'),
          renewError => console.error('Renew channel key failed: ', renewError)
        );
      }
    });

    this.client.on(ClientEvent.RemoteStreamAdded, evt => {
      const stream = evt.stream as Stream;
      this.client.subscribe(stream, { audio: true, video: true }, err => {
        console.log('Subscribe stream failed', err);
      });
    });

    this.client.on(ClientEvent.RemoteStreamSubscribed, evt => {
      const stream = evt.stream as Stream;
      const id = this.getRemoteId(stream);
      if (!this.remoteCalls.length) {
        this.remoteCalls.push(id);
        setTimeout(() => stream.play(id), 1000);
      }
    });

    this.client.on(ClientEvent.RemoteStreamRemoved, evt => {
      const stream = evt.stream as Stream;
      if (stream) {
        stream.stop();
        this.remoteCalls = [];
        console.log(`Remote stream is removed ${stream.getId()}`);
      }
    });

    this.client.on(ClientEvent.PeerLeave, evt => {
      const stream = evt.stream as Stream;
      if (stream) {
        stream.stop();
        this.remoteCalls = this.remoteCalls.filter(call => call !== `${this.getRemoteId(stream)}`);
        console.log(`${evt.uid} left from this channel`);
      }
    });
  }

  private getRemoteId(stream: Stream): string {
    return `agora_remote-${stream.getId()}`;
  }

  closePage() {
    this.client.leave(() => {
      console.log("Leavel channel successfully");
    }, (err) => {
      console.log("Leave channel failed");
    });
    this.localStream.close();
  }

  leave() {
    this.client.leave(() => {
      console.log("Leavel channel successfully");
    }, (err) => {
      console.log("Leave channel failed");
    });
    this.router.navigate(['/']);
    this.localStream.close();
  }

  mute() {
    this.client.on(ClientEvent.RemoveVideoMuted, evt => {
      console.log('video desactivado');
    })

    this.client.on(ClientEvent.PeerLeave, () => {
      console.log('eliminar a otro usuario');
    })
  };

  closeSession() {
    this.localStream.on(StreamEvent.MediaAccessDenied, () => {
      console.log('access Denied');
    });
    this.client.leave();
    this.localStream.stop();
    this.client.off;

    console.log('cerrar localStream');
  }

}
