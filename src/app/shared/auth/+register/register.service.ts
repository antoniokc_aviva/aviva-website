import { Injectable } from '@angular/core';
import { AuthService } from '../auth.service';
import { HttpClient, HttpResponse, HttpRequest } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private Authservice: AuthService, private http: HttpClient) { }

  urlBase = this.Authservice.getUrlBase();

  public data: any;

  // GET GENDERS

  userGenders() {
    return this.http
      .get(this.urlBase + 'api/v2/users/genders');
  }

  // GET TYPE DOCUMENTS

  userDocuments() {
    return this.http
      .get(this.urlBase + 'api/v2/users/documenttypes');
  }

  // GET RELATIONS
  userRelations() {
    return this.http
      .get(this.urlBase + 'api/v2/users/relations');
  }
  // GET CODE WITH EMAIL

  sendCode(email: string) {
    return this.http
      .post(this.urlBase + 'api/v2/users/validateemail/register', {
        "email": email
      });
  }

  sendCodeNew(email: string, dni: string) {
    return this.http
      .post(this.urlBase + 'api/v2/users/validateemail/register', {
        "email": email,
        "documentType": {id:1,name:"D.N.I"},
        "documentNumber":dni
      });
  }

  // REGISTER NEW USER WITH CODE

  registerNewUser(codeValida: any) {
    console.log(this.data, codeValida)
    
    return this.http
      .post(this.urlBase + 'api/v2/users/register', {
        "email": this.data.email,
        "password": this.data.password,
        "name": this.data.name,
        "surname1": this.data.surname1,
        "surname2": this.data.surname2,
        "birthdate": this.data.birthdate,
        "gender": {
          "id": this.data.gender.id,
          "name": this.data.gender.name,
        },
        "documentType": {
          "id": this.data.documentType.id,
          "name": this.data.documentType.name
        },
        "documentNumber": this.data.documentNumber,
        "phone": this.data.phone,
        "code": 1234,
        "id": this.data.id
      });
  }


}
