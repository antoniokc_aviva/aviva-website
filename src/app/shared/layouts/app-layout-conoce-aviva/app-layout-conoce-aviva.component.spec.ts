import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppLayoutConoceAvivaComponent } from './app-layout-conoce-aviva.component';

describe('AppLayoutConoceAvivaComponent', () => {
  let component: AppLayoutConoceAvivaComponent;
  let fixture: ComponentFixture<AppLayoutConoceAvivaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppLayoutConoceAvivaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppLayoutConoceAvivaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
