import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppLayoutLoginComponent } from './app-layout-login.component';

describe('AppLayoutLoginComponent', () => {
  let component: AppLayoutLoginComponent;
  let fixture: ComponentFixture<AppLayoutLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppLayoutLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppLayoutLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
