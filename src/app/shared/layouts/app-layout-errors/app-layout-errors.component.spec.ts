import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppLayoutErrorsComponent } from './app-layout-errors.component';

describe('AppLayoutErrorsComponent', () => {
  let component: AppLayoutErrorsComponent;
  let fixture: ComponentFixture<AppLayoutErrorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppLayoutErrorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppLayoutErrorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
