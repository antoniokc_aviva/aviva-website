import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppLayoutPlanesProgramasComponent } from './app-layout-planes-programas.component';

describe('AppLayoutPlanesProgramasComponent', () => {
  let component: AppLayoutPlanesProgramasComponent;
  let fixture: ComponentFixture<AppLayoutPlanesProgramasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppLayoutPlanesProgramasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppLayoutPlanesProgramasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
