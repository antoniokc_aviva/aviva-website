import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppLayoutInternasComponent } from './app-layout-internas.component';

describe('AppLayoutInternasComponent', () => {
  let component: AppLayoutInternasComponent;
  let fixture: ComponentFixture<AppLayoutInternasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppLayoutInternasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppLayoutInternasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
