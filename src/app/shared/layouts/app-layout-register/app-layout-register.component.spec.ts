import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppLayoutRegisterComponent } from './app-layout-register.component';

describe('AppLayoutRegisterComponent', () => {
  let component: AppLayoutRegisterComponent;
  let fixture: ComponentFixture<AppLayoutRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppLayoutRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppLayoutRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
