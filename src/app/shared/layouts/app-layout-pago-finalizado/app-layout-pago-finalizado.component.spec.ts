import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppLayoutPagoFinalizadoComponent } from './app-layout-pago-finalizado.component';

describe('AppLayoutPagoFinalizadoComponent', () => {
  let component: AppLayoutPagoFinalizadoComponent;
  let fixture: ComponentFixture<AppLayoutPagoFinalizadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppLayoutPagoFinalizadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppLayoutPagoFinalizadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
