import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalPlanesProgramasComponent } from './modal-planes-programas.component';

describe('ModalPlanesProgramasComponent', () => {
  let component: ModalPlanesProgramasComponent;
  let fixture: ComponentFixture<ModalPlanesProgramasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalPlanesProgramasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPlanesProgramasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
