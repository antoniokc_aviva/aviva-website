import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
MatDialogRef
@Component({
  selector: 'app-covid',
  templateUrl: './covid.component.html',
  styleUrls: ['./covid.component.scss']
})
export class CovidComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<CovidComponent>) { }

  ngOnInit() {

  }

  onClickNo(){
    this.dialogRef.close();
  }

}
