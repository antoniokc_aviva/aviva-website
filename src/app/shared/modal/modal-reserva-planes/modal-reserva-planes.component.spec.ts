import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalReservaPlanesComponent } from './modal-reserva-planes.component';

describe('ModalReservaPlanesComponent', () => {
  let component: ModalReservaPlanesComponent;
  let fixture: ComponentFixture<ModalReservaPlanesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalReservaPlanesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalReservaPlanesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
