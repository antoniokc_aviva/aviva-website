import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/auth.service';


@Component({
  selector: 'app-recovery',
  templateUrl: './recovery.component.html',
  styleUrls: ['./recovery.component.sass']
})
export class RecoveryComponent implements OnInit {

  public loaderSession: boolean = false;
  public app = 'ebooking';
  public email: string = '';
  public emailValidate: boolean;
  public validate: boolean = false;
  public readyValidate: boolean = false;
  public serviceError: boolean;
  public passValidate: boolean;
  public validatePasss: boolean;

  public ER_NUM = /^([0-9])*$/;
  public ER_STR: any = /^[A-Za-z\_\-\.\s\xF1\xD1]+$/;
  public ER_STR_MA: any = /[A-Z]/;
  public ER_EMA = /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;

  constructor(public dialogRed: MatDialogRef<RecoveryComponent>,
    public router: Router,
    @Inject(MAT_DIALOG_DATA) public message: string,
    public AuthService: AuthService) { }

  ngOnInit() {


    this.message = 'home'
    document.querySelectorAll('body')[0].classList.remove('aviva-cura-modal');
    document.querySelectorAll('body')[0].classList.remove('aviva-cuida-modal');
    document.querySelectorAll('body')[0].classList.add('aviva-home');


  }



  onClickNo() {
    this.dialogRed.close();
  }

  // VALIDATE EMAIL KEYUP && BLUR
  onkeyBlurEmail() {
    if (this.email.length > 1) {
      this.readyValidate = true;
      this.onkeyEmail();
    }
  }
  onkeyEmail() {
    this.serviceError = false;
    if (this.readyValidate) {
      if (this.validateInput(this.ER_EMA, this.email)) {
        this.emailValidate = true;
      } else {
        this.emailValidate = false;
      }
    }
  }

  // VALIDATE SIMPLE
  validateInputSim(input: any) {
    if (input.length > 3) {
      this.validatePasss = true;
      this.passValidate = true;
    } else {
      this.passValidate = false;
    }
  }

  // VALIDATE INPUTS EXPRESIONES REGULARES
  validateInput(expresion: any, input: string): boolean {
    this.validate = true;
    if (expresion.test(input)) {
      return true;
    } else {
      this.emailValidate = false;
      return false;
    }
  }

  sendRecover() {
    this.loaderSession = true;
    const dataSend = {
      email: this.email
    }
    this.AuthService.validateEmail(dataSend).subscribe((data: any) => {
      if (data.result === 'ok') {
        this.loaderSession = false;
        this.AuthService.idRecovery = data.id
        this.AuthService.emailRecovery = this.email
        console.log('mensaje en recovery:', this.message);
        this.router.navigate(['/recuperar-contraseña', this.message]);
        this.onClickNo();
      }
      console.log(data.result)
    }, (error: any) => {
      if (error.error.result === 'error') {
        this.email = '';
        this.emailValidate = false;
        this.serviceError = true;
        this.loaderSession = false;
      }
      console.log(error.error.result)
    })
    // this.router.navigate(['/recuperar-contraseña']);
    // this. onClickNo();
    console.log(this.email)
  }


}
