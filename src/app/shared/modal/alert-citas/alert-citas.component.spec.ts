import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertCitasComponent } from './alert-citas.component';

describe('AlertCitasComponent', () => {
  let component: AlertCitasComponent;
  let fixture: ComponentFixture<AlertCitasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertCitasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertCitasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
