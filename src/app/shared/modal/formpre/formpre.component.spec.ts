import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormpreComponent } from './formpre.component';

describe('FormpreComponent', () => {
  let component: FormpreComponent;
  let fixture: ComponentFixture<FormpreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormpreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormpreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
