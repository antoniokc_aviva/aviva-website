import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDetailsDoctorsComponent } from './modal-details-doctors.component';

describe('ModalDetailsDoctorsComponent', () => {
  let component: ModalDetailsDoctorsComponent;
  let fixture: ComponentFixture<ModalDetailsDoctorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalDetailsDoctorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDetailsDoctorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
