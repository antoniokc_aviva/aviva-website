import { Component, OnInit,Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { AuthService } from '../../auth/auth.service'


@Component({
  selector: 'app-modal-details-doctors',
  templateUrl: './modal-details-doctors.component.html',
  styleUrls: ['./modal-details-doctors.component.sass']
})
export class ModalDetailsDoctorsComponent implements OnInit {
  public dataDoctor;
  public urlBaseAlter

  constructor(public auth: AuthService,public dialogRed: MatDialogRef <ModalDetailsDoctorsComponent>, @Inject(MAT_DIALOG_DATA) public message: any,) { }

  ngOnInit() {
    this.urlBaseAlter = this.auth.urlBaseAlter;
    this.dataDoctor = this.message.infoDetails;
    
    if(this.message.page === 'aviva-cuida'){
      document.querySelectorAll('body')[0].classList.add('aviva-cuida-modal');
      document.querySelectorAll('body')[0].classList.remove('aviva-home');
      document.querySelectorAll('body')[0].classList.remove('aviva-cura-modal');
    }
    else if(this.message.page === 'aviva-cura'){
      document.querySelectorAll('body')[0].classList.add('aviva-cura-modal');
      document.querySelectorAll('body')[0].classList.remove('aviva-home');
      document.querySelectorAll('body')[0].classList.remove('aviva-cuida-modal');
    }else if (this.message.page === 'home'){
      document.querySelectorAll('body')[0].classList.remove('aviva-cura-modal');
      document.querySelectorAll('body')[0].classList.remove('aviva-cuida-modal');
      document.querySelectorAll('body')[0].classList.add('aviva-home', 'page-embarazo');
    }
  }

  onClickNo(){
    this.dialogRed.close();
  }

}
