import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoCancelComponent } from './no-cancel.component';

describe('NoCancelComponent', () => {
  let component: NoCancelComponent;
  let fixture: ComponentFixture<NoCancelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoCancelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoCancelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
