import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-no-cancel',
  templateUrl: './no-cancel.component.html',
  styleUrls: ['./no-cancel.component.scss']
})
export class NoCancelComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<NoCancelComponent>) { }

  ngOnInit() {
  }

  onClickNo() {
    this.dialogRef.close();
  }

}
