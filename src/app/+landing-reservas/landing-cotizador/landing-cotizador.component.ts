import { Component, OnInit,ChangeDetectorRef, AfterContentChecked } from '@angular/core';
import { LandingReservasService } from '../landing-reservas.service';

@Component({
  selector: 'app-landing-cotizador',
  templateUrl: './landing-cotizador.component.html',
  styleUrls: ['./landing-cotizador.component.sass']
})
export class LandingCotizadorComponent implements OnInit, AfterContentChecked {

  dataObs = {
    data1 : '',
    data2 : ''
  };

  constructor(private landingService : LandingReservasService, private changeDetector: ChangeDetectorRef,) { }

  ngOnInit() {
    this.landingService._activateBarProgress.subscribe(data => {
      this.dataObs = data
      console.log(this.dataObs)
    
    })
  }
  
  ngAfterContentChecked(): void {
    this.changeDetector.detectChanges();
  }

}
