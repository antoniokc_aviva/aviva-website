import { Component, OnInit } from '@angular/core';
import { LandingReservasService } from '../../landing-reservas.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-step-uno',
  templateUrl: './step-uno.component.html',
  styleUrls: ['./step-uno.component.sass']
})
export class StepUnoComponent implements OnInit {
  public mailInvalid: boolean = false;
  // NAME
  public name: string = '';
  public nameReadyValidate: boolean = false;
  public nameValidate: boolean;

   // LAST NAME
   public lastName: string = '';
   public lastNameReadyValidate: boolean = false;
   public lastNameValidate: boolean;

  public loaderSession: boolean = false;

     // NUMBER DOCUMENT
  public phoneNumber: any = '';
  public phoneReadyValidate: boolean = false;
  public phoneValidate: boolean;

  // EMAIL
  public email: string = '';
  public emailReadyValidate: boolean = false;
  public emailValidate: boolean;

   // EXPRESIONS REGULAR
   public ER_NUM = /^([0-9])*$/;
   public ER_ONLY_NUM: any = /[0-9]/;
   public ER_STR: any = /^[A-Za-z\_\-\.\s\xF1\xD1]+$/;
   public ER_STR_MA: any = /[A-Z]/;
   public ER_EMA = /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;

   slug;

   public color: any = 'accent';
   public mode: any = 'indeterminate';

   // DATA OBSERVABLE

   public dataSteps = {
     data1 : true,
     data2 : false
   }

  constructor(public landingService: LandingReservasService, private route : Router) { }

  ngOnInit() {
    this.landingService._activateBarProgress.next(this.dataSteps);
    this.slug = this.landingService.slugLanding;
    if(this.slug === null){
      this.route.navigateByUrl('/404')
    }

  }


  // VALIDATOR STRING
  onkeyValidateString(data:any): void{
    if(this.nameReadyValidate && data === this.name){
      if(data === this.name){
        this.blurValidateString(data);
      }
    }
    else if(this.lastNameReadyValidate && data === this.lastName){
      if(data === this.lastName){
        this.blurValidateString(data);
      }
    }

    else if(this.phoneReadyValidate && data === this.phoneNumber){
      if(data === this.phoneNumber){
        this.blurValidateString(data);
      }
    }

    else if(this.emailReadyValidate && data === this.email){
      if(data === this.email){
        this.blurValidateString(data);
      }
    }
  }

  blurValidateString(data:any): void {
    
   if(data === this.name){
    this.nameReadyValidate = true;
    if( this.validateInput(this.ER_STR, data)){
      this.nameValidate = true;
    }else{
      this.nameValidate = false;
    }
   }

   else if(data === this.lastName){
    this.lastNameReadyValidate = true;
    if( this.validateInput(this.ER_STR, data)){
     this.lastNameValidate = true;
   }else{
     this.lastNameValidate = false;
   }
  }

  else if(data === this.phoneNumber){
    this.phoneReadyValidate = true;
    if( this.validateInput(this.ER_NUM, data) && data.length > 1){
     this.phoneValidate = true;
   }else{
     this.phoneValidate = false;
   }
  }
   
  else if(data === this.email){
    this.emailReadyValidate = true;
    if( this.validateInput(this.ER_EMA, data)){
     this.emailValidate = true;
   }else{
     this.emailValidate = false;
   }
  }
  }

   // VALIDATE INPUTS EXPRESIONES REGULARES
 validateInput(expresion: any, input: string): boolean{
  if(expresion.test(input)){
    return true;
  }else{
    return false;
  }
}

validateAllInputs(): boolean{
  if(this.nameValidate && this.lastNameValidate && this.phoneValidate){
    return false;
  }else{
    return true;
  }
}
redirectTo(){

  this.loaderSession = true;

  const dataForm = {
    firstName: this.name,
  lastName: this.lastName,
  age: this.phoneNumber,
  email: this.email
  }
  this.landingService.sendForm1(dataForm).subscribe(data => {
    if(data.status === 'success'){
      this.landingService.dataUsuario = data.data;
      this.landingService.nameUser = this.name;
      this.landingService.emailUser = dataForm.email;
      this.route.navigateByUrl(`landing-page/${this.slug}/cotizador/resultado`);
    }
  }, error => {
    this.loaderSession = false;
    this.mailInvalid = true;
    this.email = ''
  })
  // this.route.navigateByUrl(`landing-page/${this.slug}/cotizador/resultado`)
}

   // VALIDATE PASSWORD KEYUP && BLUR
   onkeyPass() {
    console.log('keeyyyy')
    this.validateInputSim(this.phoneNumber)
    
  } 

// VALIDATE SIMPLE
validateInputSim(input: any){
    if(input.length > 1 && this.validateInput(this.ER_NUM, this.phoneNumber)){
      this.phoneValidate = true;
      this.phoneReadyValidate = true;
    }else{
      this.phoneValidate = false;
    }
}


}
