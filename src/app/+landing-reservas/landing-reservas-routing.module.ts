import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// IMPORT COMPONENT

import { LandingHomeComponent } from './landing-home/landing-home.component';
import { LandingThanksYouComponent } from './landing-thanks-you/landing-thanks-you.component';


const routes: Routes = [
  {
    path:'',
    component: LandingHomeComponent
  },
  {
    path: 'cotizador',
    loadChildren: () => import('src/app/+landing-reservas/landing-cotizador/landing-cotizador.module').then(m => m.LandingCotizadorModule)
  },
  {
    path: 'thanks-you',
    component: LandingThanksYouComponent
  }
   // {
  //   path:'',
  //   component: ReservasComponent,
  //   children: [{
  //     path: '', component: ReservasComponentContent
  //   }]
  // },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LandingReservasRoutingModule { }
