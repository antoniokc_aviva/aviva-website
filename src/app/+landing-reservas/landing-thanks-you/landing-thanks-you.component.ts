import { Component, OnInit } from '@angular/core';
import { LandingReservasService } from '../landing-reservas.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-landing-thanks-you',
  templateUrl: './landing-thanks-you.component.html',
  styleUrls: ['./landing-thanks-you.component.sass']
})
export class LandingThanksYouComponent implements OnInit {

  nameUser = '';

  constructor(private route : Router ,private landingService : LandingReservasService) { }

  ngOnInit() {
    this.nameUser = this.landingService.nameUser;
   if(this.nameUser === null){
     this.route.navigateByUrl('/404')
   }
  }

}
