import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute  } from '@angular/router';
import { LandingReservasService } from '../landing-reservas.service'

@Component({
  selector: 'app-landing-home',
  templateUrl: './landing-home.component.html',
  styleUrls: ['./landing-home.component.sass']
})
export class LandingHomeComponent implements OnInit {

  public config1 = {
    direction: 'horizontal',
    slidesPerView: 3,
    loop: false,
    spaceBetween: 8
  }

  slug: any = '';

  preloader = true;

  public color: any = 'accent';
  public mode: any = 'indeterminate';

  dataPlans;

  widthDocument = document.body.offsetWidth;

  showDesktop: boolean;

  constructor(private route: Router, private activate: ActivatedRoute, private landingReservaService: LandingReservasService) { }

  ngOnInit() {

    if (this.widthDocument > 500) {
      this.showDesktop = true;
    } else {
      this.showDesktop = false;
    }

    this.activate.params.subscribe(data => {
      this.slug = data.name;
      this.landingReservaService.slugLanding = data.name;
      this.getDataServices(this.slug);
    });
  }

  getDataServices(data: any): void {
    this.landingReservaService.getInfoPlanes(data).subscribe(data => {
      if (data.status === 'success') {
        this.landingReservaService.dataLanding = data.data
        this.dataPlans = data.data
        this.preloader = false;
      }
    }, error => {
      if (error.status === 404) {
        this.route.navigateByUrl('/404')
      }
    });
  }

  redirectTo(): void {
    this.route.navigateByUrl(`landing-page/${this.slug}/cotizador`);
  }

}
