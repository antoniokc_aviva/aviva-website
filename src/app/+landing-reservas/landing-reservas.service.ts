import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Observable ,  Subject } from 'rxjs';
import { HttpClient, HttpResponse, HttpRequest } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LandingReservasService {

  url = environment.urlApiNodos;

  slugLanding = null;

  dataLanding =  null;

  nameUser = null;

  dataUsuario = null;

  emailUser = null;

  _activateBarProgress : Subject<any> = new Subject();

  constructor(private http : HttpClient) { }

  getInfoPlanes(name: string): Observable<any>{
    return this.http.get(this.url + 'landing/' + name)
  }

  sendForm1(data:Object): Observable<any>{
    return this.http.post(this.url + 'landing/' + this.slugLanding + '/register', data);
  }

  addInformation(data: any): Observable<any>{
    return this.http
      .post(this.url + 'landing/' + this.slugLanding + '/register/' + this.dataUsuario.id + '/information-request?' , data)
  }
}
