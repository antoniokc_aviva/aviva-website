## Aviva

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.5.

## install dependece

Run `npm install`.

## running local

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## running dev

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## running prod

Run `ng build --prod`.

##################################################

## enviroments

path `src/environments`.

## 